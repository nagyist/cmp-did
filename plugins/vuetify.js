import en from 'vuetify/es5/locale/en'
import es from 'vuetify/es5/locale/es'
import colors from 'vuetify/lib/util/colors'

import DocumentIcon from '~/components/icons/oscity-documentos.vue'
import GobiernoIcon from '~/components/icons/oscity-gobierno.vue'
import IdentidadIcon from '~/components/icons/oscity-identidad.vue'
import AutenticarDni from '~/components/icons/autenticar-dni.vue'
import AutenticarAnses from '~/components/icons/autenticar-anses.vue'
import AutenticarAfip from '~/components/icons/autenticar-afip.vue'
import AutenticarMa from '~/components/icons/autenticar-ma.vue'

// PROCEDURES ICONS:
import IdCardIcon from '~/components/icons/procedures/id-card.vue'
import OpenBookIcon from '~/components/icons/procedures/open-book.vue'

export default {
  defaultAssets: {
    font: {
      family: 'Lato',
    },
  },
  customVariables: ['~/assets/variables.scss'],
  lang: {
    locales: { en, es, 'es-AR': es },
    current: 'es',
  },
  theme: {
    dark: false,
    options: { customProperties: true },
    themes: {
      light: {
        accent: colors.grey.darken3,
        info: colors.teal.lighten1,
        warning: colors.orange.lighten1,
        error: colors.red.lighten1,
        success: colors.green.lighten1,
        main: colors.grey.lighten5,
      },
    },
  },
  icons: {
    values: {
      oscity_id_card: {
        component: IdCardIcon,
      },
      oscity_open_book: {
        component: OpenBookIcon,
      },
      oscity_identidad: {
        component: IdentidadIcon,
      },
      oscity_gobierno: {
        component: GobiernoIcon,
      },
      oscity_documentos: {
        component: DocumentIcon,
      },
      oscity_autenticar_dni: {
        component: AutenticarDni,
      },
      oscity_autenticar_anses: {
        component: AutenticarAnses,
      },
      oscity_autenticar_afip: {
        component: AutenticarAfip,
      },
      oscity_autenticar_mia: {
        component: AutenticarMa,
      },
    },
  },
}
