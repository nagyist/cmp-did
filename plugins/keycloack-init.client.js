import Vue from 'vue'
import axios from 'axios'
import Keycloak from '@/static/js/keycloak'
import { signInCustomToken } from '@/services/Firebase'
import { setUserDoc } from '@/services/User'

Vue.use(Keycloak)

export default (context, inject) => {
  let keycloak = null

  const initKeycloak = (reino) => {
    let config = {}
    if (reino === 'afip') {
      config = {
        realm: context.env.AUTENTICAR_AFIP_REALM,
        url: context.env.AUTENTICAR_AFIP_URL,
        'auth-server-url': context.env.AUTENTICAR_AFIP_URL,
        clientId: context.env.AUTENTICAR_AFIP_CLIENT_ID,
        'ssl-required': 'external',
        credentials: {
          secret: context.env.AUTENTICAR_AFIP_CREDENTIAL,
        },
      }
    } else if (reino === 'anses') {
      config = {
        realm: context.env.AUTENTICAR_ANSES_REALM,
        url: context.env.AUTENTICAR_ANSES_URL,
        'auth-server-url': context.env.AUTENTICAR_ANSES_URL,
        clientId: context.env.AUTENTICAR_ANSES_CLIENT_ID,
        'ssl-required': 'external',
        credentials: {
          secret: context.env.AUTENTICAR_ANSES_CREDENTIAL,
        },
      }
    } else if (reino === 'mi-argentina') {
      config = {
        realm: context.env.AUTENTICAR_MI_ARGENTINA_REALM,
        url: context.env.AUTENTICAR_MI_ARGENTINA_URL,
        'auth-server-url': context.env.AUTENTICAR_MI_ARGENTINA_URL,
        clientId: context.env.AUTENTICAR_MI_ARGENTINA_CLIENT_ID,
        'ssl-required': 'external',
        credentials: {
          secret: context.env.AUTENTICAR_MI_ARGENTINA_CREDENTIAL,
        },
      }
    } else if (reino === 'renaper') {
      config = {
        realm: context.env.AUTENTICAR_RENAPER_REALM,
        url: context.env.AUTENTICAR_RENAPER_URL,
        'auth-server-url': context.env.AUTENTICAR_RENAPER_URL,
        clientId: context.env.AUTENTICAR_RENAPER_CLIENT_ID,
        'ssl-required': 'external',
        credentials: {
          secret: context.env.AUTENTICAR_RENAPER_CREDENTIAL,
        },
      }
    }

    keycloak = Keycloak(config)
  }

  const getUserByUid = async (uid) => {
    try {
      const userExist = await axios.post(
        `${context.env.OSCITY_ENDPOINTS_URL}/userEmailExist`,
        {
          uid,
        }
      )
      return userExist
    } catch (error) {
      if (error.response?.status === 404) {
        return null
      } else {
        context.redirect('/server-error')
      }
    }
  }

  const reino = localStorage.getItem('reino')
  const backToReino = localStorage.getItem('backToReino')

  if (reino) {
    if (backToReino) {
      initKeycloak(reino)
      keycloak
        .init({
          checkLoginIframe: false,
          onLoad: 'check-sso',
        })
        .success((auth) => {
          if (auth) {
            keycloak
              .loadUserInfo()
              .success(async (res) => {
                const autenticarUser = {
                  ...res,
                  cuit: res.cuit ? res.cuit : res.preferred_username,
                }
                const user = await getUserByUid(
                  res.cuit ? res.cuit : res.preferred_username
                )
                if (!user || user?.data?.user?.providerData.length === 0) {
                  axios
                    .post(
                      `${context.env.OSCITY_ENDPOINTS_URL}/createCustomToken`,
                      {
                        uid: autenticarUser.cuit,
                      }
                    )
                    .then((customToken) => {
                      signInCustomToken(context.$fire, {
                        credential: customToken.data.customToken,
                      })
                        .then(() => {
                          const autenticarData = {
                            first_name: res.given_name
                              ? res.given_name
                              : res.family_name,
                            autenticated: reino,
                            last_name: res.family_name ? res.family_name : '',
                            activated: false,
                            dni: autenticarUser.cuit,
                            levelCidi:
                              reino === 'afip' && ['3', '4'].includes(res.nivel)
                                ? 2
                                : 1,
                            tipoPersona: ['F', 'FISICA'].includes(
                              res.tipo_persona
                            )
                              ? 'f'
                              : 'j',
                          }
                          setUserDoc(
                            context.$fire,
                            autenticarUser.cuit,
                            autenticarData
                          )
                          context.store.dispatch('setUserAutenticar', {
                            autenticar: {
                              ...autenticarData,
                              uid: autenticarUser.cuit,
                            },
                          })
                          context.store.commit('users/SET_USER', autenticarData)
                          setTimeout(() => {
                            context.redirect('/complete-data')
                          }, 1000)
                          localStorage.removeItem('backToReino')
                        })
                        .catch(() => {
                          localStorage.removeItem('reino')
                          localStorage.removeItem('backToReino')
                        })
                    })
                    .catch(() => {
                      localStorage.removeItem('reino')
                      localStorage.removeItem('backToReino')
                    })
                } else {
                  localStorage.setItem('cuit', res.cuit)
                  if (reino === 'afip' && ['3', '4'].includes(res.nivel)) {
                    localStorage.setItem('levelUpdated', 'true')
                    const autenticarData = {
                      levelCidi: 2,
                      autenticated: reino,
                      tipoPersona: ['F', 'FISICA'].includes(res.tipo_persona)
                        ? 'f'
                        : 'j',
                    }
                    setUserDoc(
                      context.$fire,
                      autenticarUser.cuit,
                      autenticarData
                    )
                    context.store.dispatch('setUserAutenticar', {
                      autenticar: {
                        ...autenticarData,
                        uid: autenticarUser.cuit,
                      },
                    })
                    context.store.commit('users/SET_USER', autenticarData)
                  }
                }
              })
              .error(() => {
                localStorage.removeItem('reino')
                localStorage.removeItem('backToReino')
                localStorage.removeItem('levelUpdated')
              })
          } else {
            localStorage.removeItem('reino')
            localStorage.removeItem('backToReino')
            localStorage.removeItem('levelUpdated')
          }
        })
        .error(() => {
          localStorage.removeItem('reino')
          localStorage.removeItem('backToReino')
          localStorage.removeItem('levelUpdated')
        })
    } else if (!context.store.getters.loggedIn) {
      localStorage.removeItem('reino')
    }
  } else {
    localStorage.removeItem('reino')
    localStorage.removeItem('backToReino')
    localStorage.removeItem('levelUpdated')
  }

  inject('autenticar', {
    init: (reino) => {
      localStorage.setItem('reino', reino)
      localStorage.setItem('backToReino', true)
      initKeycloak(reino)
      keycloak
        .init({
          onLoad: 'login-required',
          checkLoginIframe: false,
        })
        .success(() => {})
        .error(() => {
          localStorage.removeItem('reino')
          localStorage.removeItem('backToReino')
        })
    },
    logout: (reino) => {
      if (!reino) {
        reino = localStorage.getItem('reino')
      }
      initKeycloak(reino)
      keycloak.init({
        checkLoginIframe: false,
        onLoad: 'check-sso',
      })
      keycloak.logout()
    },
  })
}
