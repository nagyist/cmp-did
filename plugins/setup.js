export default (context) => {
  context.store.dispatch('authconfig/get')
  context.store.dispatch('landingtext/get')
  context.store.dispatch('brand/get')
  context.store.dispatch('setting/get')
  context.store.dispatch('certsConfig/get')
  context.store.dispatch('setHost')
  context.store.dispatch('procedures/get')
}
