let baseURL = 'http://localhost:8080/_ah/api/oscity/v1'
if (process.env.OSCITY_ENDPOINTS_URL) {
  baseURL = process.env.OSCITY_ENDPOINTS_URL
}
if (process.env.NODE_ENV === 'production') {
  baseURL = process.env.OSCITY_ENDPOINTS_URL
}

export default {
  loadingIndicator: {
    name: 'folding-cube',
    color: '#EC268F',
    background: 'white',
    width: '200px',
  },
  ssr: false,
  publicRuntimeConfig: {
    baseURL: process.env.OSCITY_URL,
  },
  privateRuntimeConfig: {
    googleApiKey: process.env.GOOGLE_API_KEY,
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    htmlAttrs: {
      lang: process.env.FIREBASE_PROJECT_ID === 'unicef-certs' ? 'en' : 'es',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      {
        rel: 'stylesheet',
        type: 'text/css',
        href: 'https://unpkg.com/leaflet@1.5.1/dist/leaflet.css',
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&family=Montserrat:wght@400;600;700&family=Poppins:wght@400;500;600;700;900&family=Roboto:wght@500&display=swap',
      },
      {
        rel: 'icon',
        type: 'image/png',
        href: 'https://firebasestorage.googleapis.com/v0/b/demosburg-dev.appspot.com/o/brand%2Fimages%2Fempty_icon.png?alt=media&token=4964eaed-f142-4083-9aaf-bfd4c7790449',
      },
    ],
    script: [
      {
        src: 'https://unpkg.com/leaflet@1.5.1/dist/leaflet.js',
        integrity:
          'sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==',
        crossorigin: '',
      },
      {
        src:
          'https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=' +
          process.env.GOOGLE_API_KEY,
      },
      {
        src: 'https://unpkg.com/uport-connect/dist/uport-connect.js',
      },
      {
        src:
          'https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key=' +
          process.env.GOOGLE_API_KEY,
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/style/app.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/setup.js',
    '@/plugins/vue-tel-input.js',
    '@/plugins/vue-easy-loading.js',
    '@/plugins/keycloack-init.client.js',
    '@/plugins/logrocket.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/composition-api',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://sentry.nuxtjs.org/guide/setup/
    process.env.NODE_ENV === 'production'
      ? [
          '@nuxtjs/sentry',
          {
            dsn: 'https://1d43171586b44af9bc6265349db2c417@o282504.ingest.sentry.io/5672401',
            config: {
              disabled: false,
              environment: 'production',
            }, // Additional config
          },
        ]
      : ['@nuxtjs/sentry', {}],
    [
      '@nuxtjs/dotenv',
      {
        only: [
          'NODE_ENV',
          'LOGROCKET_ID',
          'EMAIL_NAME',
          'INFURA_KEY_ROPSTEN',
          'GOOGLE_API_KEY',
          'OSCITY_ENDPOINTS_URL',
          'FIREBASE_API_KEY',
          'FIREBASE_AUTH_DOMAIN',
          'FIREBASE_DATABASE_URL',
          'FIREBASE_PROJECT_ID',
          'FIREBASE_STORAGE_BUCKET',
          'FIREBASE_MESSAGING_SENDER_ID',
          'FIREBASE_APP_ID',
          'AUTENTICAR_AFIP_REALM',
          'AUTENTICAR_AFIP_URL',
          'AUTENTICAR_AFIP_CLIENT_ID',
          'AUTENTICAR_AFIP_CREDENTIAL',
          'AUTENTICAR_AFIP_ENDPOINTS_URL',
          'AUTENTICAR_MI_ARGENTINA_REALM',
          'AUTENTICAR_MI_ARGENTINA_URL',
          'AUTENTICAR_MI_ARGENTINA_CLIENT_ID',
          'AUTENTICAR_MI_ARGENTINA_CREDENTIAL',
          'AUTENTICAR_MI_ARGENTINA_ENDPOINTS_URL',
          'AUTENTICAR_ANSES_REALM',
          'AUTENTICAR_ANSES_URL',
          'AUTENTICAR_ANSES_CLIENT_ID',
          'AUTENTICAR_ANSES_CREDENTIAL',
          'AUTENTICAR_RENAPER_REALM',
          'AUTENTICAR_RENAPER_URL',
          'AUTENTICAR_RENAPER_CLIENT_ID',
          'AUTENTICAR_RENAPER_CREDENTIAL',
          'AUTENTICAR_ANSES_ENDPOINTS_URL',
          'CITY_NAME',
        ],
      },
    ],
    '@nuxtjs/axios',
    'nuxt-i18n',

    [
      /**
       * Firebase configuration
       */
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: process.env.FIREBASE_API_KEY,
          authDomain: process.env.FIREBASE_AUTH_DOMAIN,
          databaseURL: process.env.FIREBASE_DATABASE_URL,
          projectId: process.env.FIREBASE_PROJECT_ID,
          storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
          messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
          appId: process.env.FIREBASE_APP_ID,
          measurementId: process.env.FIREBASE_MEASUREMENT_ID,
        },
        services: {
          auth: {
            persistence: 'local', // default
            initialize: {
              onAuthStateChangedMutation: 'ON_AUTH_STATE_CHANGED_MUTATION',
              onAuthStateChangedAction: 'users/fetchUser',
              subscribeManually: false,
            },
            ssr: {
              ignorePaths: [
                '/admin', // path is ignored if url.pathname.startsWith('/admin')
                /^api/, // path is ignored if url.pathname without the leading slash (/) matches the RegExp
              ],
              credential: true, // retrieved credentials from GOOGLE_APPLICATION_CREDENTIALS env variable
            },
            // emulatorPort:
            //   process.env.NODE_ENV === 'development' ? 9099 : undefined,
            // emulatorHost: 'http://localhost',
          },
          firestore: true,
          functions: true,
          storage: true,
          database: true,
          messaging: true,
          performance: true,
          analytics: true,
          remoteConfig: true,
        },
      },
    ],
  ],

  i18n: {
    strategy: 'no_prefix',
    defaultLocale:
      process.env.FIREBASE_PROJECT_ID === 'unicef-certs' ? 'en' : 'es',
    lazy: true,
    langDir: 'locales/',
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: `i18n_redirected_${process.env.FIREBASE_PROJECT_ID}`,
      fallbackLocale: 'en',
      alwaysRedirect: process.env.FIREBASE_PROJECT_ID !== 'unicef-certs',
    },
    locales: [
      {
        iso: 'en-US',
        file: 'en.json',
        text: 'EN',
        value: 'en',
        code: 'en',
        name: 'English',
      },
      {
        iso: 'es-MX',
        file: 'es.json',
        text: 'ES',
        value: 'es',
        code: 'es',
        name: 'Español',
      },
      {
        iso: 'es-AR',
        file: 'es-AR.json',
        text: 'ES-AR',
        value: 'es-AR',
        code: 'es-AR',
        name: 'Español Argentino',
      },
    ],
    vueI18n: {
      locale: process.env.FIREBASE_PROJECT_ID === 'unicef-certs' ? 'en' : 'es',
      fallbackLocale: 'en',
    },
  },
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL,
    retry: false,
    debug: true,
    prefix: '/api',
    credentials: true,
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    optionsPath: '~/plugins/vuetify.js',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extend(config) {
      config.node = {
        fs: 'empty',
      }
    },
  },
}
