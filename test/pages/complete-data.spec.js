import Vuex from 'vuex'
import Vuetify from 'vuetify'
import { createLocalVue, shallowMount, createWrapper } from '@vue/test-utils'
import VueRouter from 'vue-router'
import swal from 'sweetalert'
import axios from 'axios'

import CompleteData from '~/pages/complete-data.vue'
import QrRegister from '@/components/Register/QrRegister.vue'
import ArgRegister from '@/components/Register/ArgRegister.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
let vuetify

jest.mock('sweetalert')
jest.mock('axios')

const router = new VueRouter({
  routes: [
    {
      path: '/complete-data',
      name: 'completeData',
    },
    {
      path: '/',
      name: 'home',
    },
  ],
})

// document.body.setAttribute('data-app', true)

const $fire = {
  auth: {
    currentUser: {
      updateProfile: jest.fn(),
      updatePassword: jest.fn(),
      updateEmail: jest.fn(),
    },
    signInWithCustomToken: jest.fn(),
  },
  firestore: {
    collection: jest.fn(() => {
      return {
        add: jest.fn().mockResolvedValue({ id: '1389ada779h' }),
        doc: jest.fn(() => {
          return {
            set: jest.fn(),
          }
        }),
      }
    }),
  },
}

const $fireModule = {
  storage: jest.fn(() => {
    return {
      ref: jest.fn(() => {
        return {
          child: jest.fn(() => {
            return {
              put: jest.fn(),
            }
          }),
        }
      }),
    }
  }),
}

const originalEnv = process.env

const flushPromises = () => new Promise((resolve) => setTimeout(resolve, 0))

// eslint-disable-next-line import/no-named-as-default-member
const store = new Vuex.Store({
  mutations: {
    'users/SET_USER': jest.fn(),
  },
  state: {
    users: {
      user: { dni: '87381278', uid: '97sd9d' },
    },
  },
  getters: {
    loggedIn: () => {
      return true
    },
    'certsConfig/certsConfig': () => {
      return {
        identity: 'info@os.city',
        name: 'Certificados Os City',
        pubkey: '0xa854cc517c45c806073e1ca7fbd082b63837e332',
        manualUpload: false,
        dataLoadingByForm: false,
        certsCollectionsToRevoke: ['certificates'],
        withAuthorized: false,
      }
    },
    'users/user': () => {
      return {
        first_name: 'Pepe',
        last_name: 'Aguilar',
        email: 'joker@email.com',
        password: '****',
        dni: '234546',
        public_address: 'sdf8sa6df7yuhbn',
        type: 'superadmin',
        uid: '97sd9d',
      }
    },
    'brand/brand': () => {
      return {
        header_background_type: '',
      }
    },
  },
})

describe('Test complete-data changing steps methods and computed properties', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('The computed property public_adress should return the wallet that is linked', () => {
    // eslint-disable-next-line import/no-named-as-default-member

    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          wallet: 'asd6asduh3n',
        }
      },
    })

    expect(wrapper.vm.public_address).toBe('asd6asduh3n')
  })

  it('The computed property dni should return the dni of the user', () => {
    // eslint-disable-next-line import/no-named-as-default-member

    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          wallet: 'asd6asduh3n',
        }
      },
    })

    expect(wrapper.vm.dni).toBe('38127')
  })

  it('The computed property stepTransformed should return 3 if the step variable is equal to 2', () => {
    // eslint-disable-next-line import/no-named-as-default-member

    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          step: 2,
        }
      },
    })

    expect(wrapper.vm.stepTransformed).toBe(3)
  })

  it('The computed property stepTransformed should return 2 if the step variable is equal to 3', () => {
    // eslint-disable-next-line import/no-named-as-default-member

    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          step: 3,
        }
      },
    })

    expect(wrapper.vm.stepTransformed).toBe(2)
  })

  it('The computed property stepTransformed should return 1 if the step variable is equal to 4', () => {
    // eslint-disable-next-line import/no-named-as-default-member

    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          step: 4,
        }
      },
    })

    expect(wrapper.vm.stepTransformed).toBe(1)
  })

  it('The computed property stepTransformed should return the substraction of 5 and the step in the route query (if it exists)', () => {
    router.push({ name: 'completeData', query: { step: 2 } })

    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          step: null,
        }
      },
    })

    expect(wrapper.vm.stepTransformed).toBe(3)
  })

  it("The computed property stepTransformed should return the 2 if route query doesn't exists", () => {
    router.push({ name: 'completeData', query: { step: null } })

    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          step: null,
        }
      },
    })

    expect(wrapper.vm.stepTransformed).toBe(2)
  })

  it('beforeRouteLeave method should called a swal before you change routes if the user has not finished registering. If clicked on yes, it should emit on root', async () => {
    router.push({ name: 'completeData', query: { step: 2 } })

    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,
      swal,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          step: 2,
        }
      },
    })

    wrapper.setData({ registerComplete: false })

    // console.log(wrapper.vm.$options.beforeRouteLeave[0])

    const beforeRouteUpdate = wrapper.vm.$options.beforeRouteLeave[0]

    swal.mockImplementation(() => Promise.resolve(true))

    beforeRouteUpdate.call(wrapper.vm, 'toObj', 'fromObj', jest.fn())

    expect(swal).toHaveBeenCalledTimes(1)
    expect(swal).toHaveBeenCalledWith({
      title: 'No has completado tu registro',
      text: '¿Estas seguro que quieres abandonar sin terminar tu registro?.',
      icon: 'warning',
      buttons: ['Cancelar', 'Si, terminaré después'],
    })

    await wrapper.vm.$nextTick()

    const rootWrapper = createWrapper(wrapper.vm.$root)
    expect(rootWrapper.emitted().logout).toBeTruthy()
  })

  it('beforeRouteLeave method should called a swal before you change routes if the user has not finished registering. If clicked on no, you stay on the same page', async () => {
    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,
      swal,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          step: 2,
        }
      },
    })

    wrapper.setData({ registerComplete: false })

    const beforeRouteUpdate = wrapper.vm.$options.beforeRouteLeave[0]

    swal.mockImplementation(() => Promise.resolve(false))

    const next = jest.fn()

    beforeRouteUpdate.call(wrapper.vm, 'toObj', 'fromObj', next)

    expect(swal).toHaveBeenCalledTimes(1)
    expect(swal).toHaveBeenCalledWith({
      title: 'No has completado tu registro',
      text: '¿Estas seguro que quieres abandonar sin terminar tu registro?.',
      icon: 'warning',
      buttons: ['Cancelar', 'Si, terminaré después'],
    })

    await wrapper.vm.$nextTick()

    const rootWrapper = createWrapper(wrapper.vm.$root)
    expect(rootWrapper.emitted().logout).toBeFalsy()
    expect(next).toHaveBeenCalledTimes(1)
    expect(next).toHaveBeenCalledWith(false)
  })

  it('beforeRouteLeave method should not call a swal if the user has finished registering', async () => {
    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,
      swal,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          step: 2,
        }
      },
    })

    wrapper.setData({ registerComplete: true })

    const beforeRouteUpdate = wrapper.vm.$options.beforeRouteLeave[0]

    swal.mockImplementation(() => Promise.resolve(false))

    const next = jest.fn()

    beforeRouteUpdate.call(wrapper.vm, 'toObj', 'fromObj', next)

    expect(swal).not.toHaveBeenCalled()

    await wrapper.vm.$nextTick()

    const rootWrapper = createWrapper(wrapper.vm.$root)
    expect(rootWrapper.emitted().logout).toBeFalsy()
    expect(next).toHaveBeenCalledTimes(1)
  })

  it('When the Change Wallet button is pressed, the disconnectWallet method should be called', async () => {
    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,
      swal,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          step: 2,
        }
      },
    })

    const changeWalletSpy = jest.spyOn(wrapper.vm, 'disconnectWallet')

    wrapper.findComponent(QrRegister).vm.$emit('disconnectWallet')

    await wrapper.vm.$nextTick()

    expect(changeWalletSpy).toHaveBeenCalledTimes(1)
  })

  it('When the Wallet is connected, the updateSteps method should be called', async () => {
    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,
      swal,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          step: 2,
        }
      },
    })

    const collectWalletSpy = jest.spyOn(wrapper.vm, 'updateSteps')

    wrapper.findComponent(QrRegister).vm.$emit('showSteps', false)

    await wrapper.vm.$nextTick()

    expect(collectWalletSpy).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.showSteps).toBe(false)
  })

  it('Changing steps changes the number shown in the header of the page', () => {
    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,
      swal,

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          isValid: true,
        }
      },
    })

    wrapper.vm.changeStep(2)

    const headerText = wrapper.find('#textSteps')

    expect(headerText.text()).toBe(`Estás a 3
        pasos de tu
        identidad digital`)
  })
})

describe('Test complete-data register methods', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
    process.env = {
      ...originalEnv,
      OSCITY_ENDPOINTS_URL: 'http:link',
    }
  })

  afterEach(() => {
    jest.clearAllMocks()
    process.env = originalEnv
  })

  it('When clicking register in step 2, it should change to step 3 and call axios', async () => {
    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,
      swal,

      mocks: {
        $t: (msg) => msg,
        axios,
        $fire,
      },

      data() {
        return {
          step: 2,
          urlEndpoints: 'http:link',
        }
      },
    })

    const dataToRegister = {
      first_name: 'Ren',
      last_name: 'Amamiya',
      email: 'joker@hotmail.com',
      password: '****',
      dni: '14523',
      public_address: '01x12j3u8dhjmgfdg',
    }

    axios.post.mockImplementation(() =>
      Promise.resolve({ status: 200, data: { customToken: '123123' } })
    )

    const submitRegisterSpy = jest.spyOn(wrapper.vm, 'onSubmit')

    wrapper.findComponent(ArgRegister).vm.$emit('data', dataToRegister)

    await wrapper.vm.$nextTick()

    expect(axios.post).toHaveBeenCalledWith('http:link/createCustomToken', {
      uid: '14523',
    })

    await flushPromises()

    expect(submitRegisterSpy).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.step).toBe(3)
    expect(wrapper.vm.loading).toBe(false)
  })

  it('If the password is to weak, it should display a swal with that error', async () => {
    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,
      swal,

      mocks: {
        $t: (msg) => msg,
        axios,
        $fire,
      },

      data() {
        return {
          step: 2,
          urlEndpoints: 'http:link',
        }
      },
    })

    const dataToRegister = {
      first_name: 'Ren',
      last_name: 'Amamiya',
      email: 'joker@hotmail.com',
      password: '',
      dni: '14523',
      public_address: '01x12j3u8dhjmgfdg',
    }

    const consoleErrorMock = jest.spyOn(console, 'error').mockImplementation()

    const err = new Error('Error test - Weak Password')
    err.code = 'auth/weak-password'

    axios.post.mockImplementation(() => Promise.reject(err))

    const submitRegisterSpy = jest.spyOn(wrapper.vm, 'onSubmit')

    wrapper.findComponent(ArgRegister).vm.$emit('data', dataToRegister)

    await wrapper.vm.$nextTick()

    expect(axios.post).toHaveBeenCalledWith('http:link/createCustomToken', {
      uid: '14523',
    })

    await flushPromises()

    expect(submitRegisterSpy).toHaveBeenCalledTimes(1)
    expect(swal).toHaveBeenCalledTimes(1)
    expect(swal).toHaveBeenCalledWith({
      title: 'La contraseña debe tener al menos 6 caracteres.',
      icon: 'warning',
      button: 'Reintentar',
    })
    expect(wrapper.vm.loading).toBe(false)
    expect(consoleErrorMock).toHaveBeenCalledTimes(1)
    consoleErrorMock.mockRestore()
  })

  it('If the email is already registered, it should display a swal with that error', async () => {
    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,
      swal,

      mocks: {
        $t: (msg) => msg,
        axios,
        $fire,
      },

      data() {
        return {
          step: 2,
          urlEndpoints: 'http:link',
        }
      },
    })

    const dataToRegister = {
      first_name: 'Ren',
      last_name: 'Amamiya',
      email: 'joker@hotmail.com',
      password: '',
      dni: '14523',
      public_address: '01x12j3u8dhjmgfdg',
    }

    const consoleErrorMock = jest.spyOn(console, 'error').mockImplementation()

    const err = new Error('Error test - Emmail already in use')
    err.code = 'auth/email-already-in-use'

    axios.post.mockImplementation(() => Promise.reject(err))

    const submitRegisterSpy = jest.spyOn(wrapper.vm, 'onSubmit')

    wrapper.findComponent(ArgRegister).vm.$emit('data', dataToRegister)

    await wrapper.vm.$nextTick()

    expect(axios.post).toHaveBeenCalledWith('http:link/createCustomToken', {
      uid: '14523',
    })

    await flushPromises()

    expect(submitRegisterSpy).toHaveBeenCalledTimes(1)
    expect(swal).toHaveBeenCalledTimes(1)
    expect(swal).toHaveBeenCalledWith({
      title: 'El correo ya está en uso por otra cuenta.',
      icon: 'warning',
      button: 'Reintentar',
    })
    expect(wrapper.vm.loading).toBe(false)
    expect(consoleErrorMock).toHaveBeenCalledTimes(1)
    consoleErrorMock.mockRestore()
  })
})

describe('Test complete-data certification methods', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
    process.env = {
      ...originalEnv,
      OSCITY_ENDPOINTS_URL: 'http:link',
    }
  })

  afterEach(() => {
    // jest.useRealTimers()
    jest.clearAllMocks()
    process.env = originalEnv
  })

  it('When the Wallet is connected, the countdown for certification starts and calls the certification method', async () => {
    jest.useFakeTimers()

    const wrapper = shallowMount(CompleteData, {
      localVue,
      vuetify,
      store,
      router,
      swal,

      mocks: {
        $t: (msg) => msg,
        axios,
        $fire,
        $fireModule,
      },

      data() {
        return {
          data: {
            first_name: 'Ren',
            last_name: 'Amamiya',
            email: 'joker@hotmail.com',
            password: '****',
            dni: '14523',
            public_address: '01x12j3u8dhjmgfdg',
          },
          step: 2,
          urlEndpoints: 'http:link',
        }
      },
    })

    const certTimetoutSpy = jest.spyOn(wrapper.vm, 'certificateWithTimeout')
    const certCUSpy = jest.spyOn(wrapper.vm, 'certificateChangeUser')
    const certificateDSpy = jest.spyOn(wrapper.vm, 'certificateData')

    wrapper
      .findComponent(QrRegister)
      .vm.$emit('certificateData', '01x12j3u8dhjmgfdg')

    jest.advanceTimersByTime(15000)

    await wrapper.vm.$nextTick()

    expect(certTimetoutSpy).toBeCalledTimes(1)
    expect(certCUSpy).toHaveBeenCalledTimes(1)
    expect(certificateDSpy).toHaveBeenCalledTimes(1)
  })
})
