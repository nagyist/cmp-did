import { createLocalVue, mount } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

import TermsOfService from '@/pages/terms-of-service.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
let vuetify
let wrapper

// eslint-disable-next-line import/no-named-as-default-member
const $store = new Vuex.Store({
  state: {},
  getters: {
    'brand/brand': () => {
      return { termsAndConditions: 'Terms of service text' }
    },
  },
})

describe('Terms of Service', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  it('Expect brand to have the terms of service indicated in the store', () => {
    wrapper = mount(TermsOfService, {
      localVue,
      vuetify,
      mocks: {
        $store,
        $t: (msg) => msg,
      },
    })
    expect(wrapper.vm.brand.termsAndConditions).toBe('Terms of service text')
  })
})
