import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import { state, mutations, getters } from '~/store/authconfig.js'

import Login from '~/pages/Login.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

// Mock de localStorage
const localStorageMock = (function () {
  let store = {}

  return {
    getItem(key) {
      return store[key]
    },

    setItem(key, value) {
      store[key] = value
    },

    clear() {
      store = {}
    },

    removeItem(key) {
      delete store[key]
    },

    getAll() {
      // eslint-disable-next-line no-console
      console.log(store)
    },
  }
})()

Object.defineProperty(window, 'localStorage', { value: localStorageMock })

// eslint-disable-next-line import/no-named-as-default-member
const store = new Vuex.Store({
  modules: {
    authconfig: {
      namespaced: true,
      state,
      mutations,
      getters,
    },
  },
})

const $autenticar = {
  logout: jest.fn(),
}

describe('Check pages/Login functionalities', () => {
  let vuetify
  let wrapper

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
    window.localStorage.clear()
  })

  it('When the component is mounted, it logouts the user and clears variables in localStorage', async () => {
    // Define the localStorage variables that will be cleaned in mounted
    window.localStorage.setItem('reino', 'r31n0')
    window.localStorage.setItem('backToReino', 'b4ckT0R31n0')
    window.localStorage.setItem('cuit', 'cu1T')

    expect(localStorage.getItem('reino')).toBe('r31n0')
    expect(localStorage.getItem('backToReino')).toBe('b4ckT0R31n0')
    expect(localStorage.getItem('cuit')).toBe('cu1T')

    wrapper = await shallowMount(Login, {
      localVue,
      vuetify,
      store,

      directives: {
        load() {},
      },

      mocks: {
        $t: (msg) => msg,
        $autenticar,
      },
    })

    expect(wrapper.vm.walletType).toBe('')
    expect($autenticar.logout).toBeCalledTimes(1)
    expect(localStorage.getItem('reino')).toBeUndefined()
    expect(localStorage.getItem('backToReino')).toBeUndefined()
    expect(localStorage.getItem('cuit')).toBeUndefined()
  })

  it('Clicking the "Back" button triggers the method clearData correctly', async () => {
    wrapper = await shallowMount(Login, {
      localVue,
      vuetify,
      store,

      computed: {
        loading() {
          return false
        },
      },

      directives: {
        load() {},
      },

      mocks: {
        $t: (msg) => msg,
        $autenticar,
      },

      data() {
        return {
          wallet: 'true',
          walletType: 'Coinbase',
          walletLink: {
            disconnect: jest.fn(),
          },
        }
      },
    })

    const backSpy = jest.spyOn(wrapper.vm, 'clearData')

    const backBtn = wrapper.find('#backBtn')
    backBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(backSpy).toBeCalledTimes(1)
    expect(wrapper.vm.wallet).toBe('')
    expect(wrapper.vm.walletType).toBe('')
  })
})
