import Vuetify from 'vuetify'
import Vuex, { Store } from 'vuex'
import VueRouter from 'vue-router'
import { mount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import digitalCitizen from '@/pages/digital-citizen.vue'

const emptyBrand = {
  app_name: '',
  city_splash_primary:
    'https://firebasestorage.googleapis.com/v0/b/demosburg-dev.appspot.com/o/brand%2Fimages%2Fdefault_splash_primary.png?alt=media&token=b4feafa8-08ba-4396-9fc0-1fbbed2cf5e4',
  city_splash_secondary:
    'https://firebasestorage.googleapis.com/v0/b/demosburg-dev.appspot.com/o/brand%2Fimages%2Fdefault_splash_secondary.png?alt=media&token=616244e7-e306-498a-a3b3-3d138c15f10f',
  city_splash_login: '',
  city_url: 'demo.os.city',
  city_name: '',
  copyright_footer: '',
  favicon: '',
  logo: null,
  logo_provincia_misiones: null,
  primary_color: '#e5528e',
  secondary_color: '#ad57b2',
  header_color: '#ffffff',
  footer_color: '#ffffff',
  footer_contact_button_color: '',
  footer_background_type: 'lineal',
  footer_bg_primary: '',
  footer_bg_secondary: '',
  footer_logo: '',
  footer_text_color: '',
  textHeaderColor: '#ffffff',
  textFooterColor: '#ffffff',
  heightLogo: '40px',
  additional_color: '#d6d6d6',
  about_title: 'Acerca de',
  about_content: '',
  contact_title: 'Contacto',
  contact_info: '',
  hero_bg_primary: '#5d306f',
  hero_bg_secondary: '#bd0b4d',
  terms: false,
  privacy: false,
  termsAndConditions: '',
  privayPolicy: '',
  needsInfo: true,
  buttonColorLogin: '#43475C',
  mobileStepThree: '',
  firstImageStepThree: '',
  secondImageStepThree: '',
  thirdImageStepThree: '',
  header_bg_primary: '#0096da',
  header_bg_secondary: '#035d98',
  header_button_color: '#0088CC',
  header_background_type: 'lineal',
  errorImage: '',
  hero_background_type: 'lineal',
  hero_bg_image: '',
  levelUpImage: '',
  validateLevelImage: '',
  appStoreDowload: '',
  qrImage: '',
  loaded: false,
  avatar_text_color: '#FFFFFF',
}

const emptyLandingText = {
  heroSection: {
    title: '',
    subtitle: '',
    second_subtitle: '',
  },
  headerSection: {
    login_button: 'Iniciar sesión',
    register_button: 'Registro',
  },
  columnsSection: {
    city: '',
    slogan: '',
    title_column1: '',
    title_column2: '',
    title_column3: '',
    info_column1: '',
    info_column2: '',
    info_column3: '',
  },
  reminderSection: {
    reminderTitle: '',
    reminderText: '',
  },
  certificateSection: {
    certificate_title: '',
  },
  digitalCitizen: {
    title: '',
    subtitle: '',
    levels_description: '',
    step_1: {
      description: '',
    },
    step_2: {
      description: '',
    },
    step_3: {
      description: '',
    },
    step_4: {
      description: '',
    },
    step_5: {
      description: '',
    },
  },
  whatIsCidi: {
    section_1: {
      title: '',
      description: '',
    },
    section_2: {
      title: '',
      description: '',
    },
    section_3: {
      title: '',
      description: '',
    },
    section_4: {
      title: '',
      description: '',
    },
  },
  step_number_color: '',
  loaded: false,
}

const createStore = (params) => {
  return new Store({
    state: {
      brand: {
        ...emptyBrand,
        ...params?.brand,
      },
      landingtext: {
        ...emptyLandingText,
        ...params?.landingText,
      },
    },

    getters: {
      'brand/brand': (state) => state.brand,
      'landingtext/landingtext': (state) => state.landingtext,
    },
    mutations: {
      'brand/SET_BRAND': (state, brand) => {
        state.brand = brand
      },
      'landingtext/SET_LANDINGTEXT': (state, landingtext) => {
        state.landingtext = { ...state.landingText, ...landingtext }
      },
    },
    // actions: {
    //   'users/fetchUser': () => {
    //     return jest.fn()
    //   },
    //   setloggedIn: () => {
    //     return jest.fn()
    //   },
    // },
  })
}

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
document.body.setAttribute('data-app', true)

describe('Testing Digital Citizen Page', () => {
  let vuetify
  let wrapper
  let store

  const router = new VueRouter()

  beforeEach(() => {
    vuetify = new Vuetify()
  })
  afterEach(() => {
    wrapper.destroy()
    window.localStorage.clear()
  })

  test('Should mount section component succesfully', async () => {
    store = createStore()

    wrapper = await mount(digitalCitizen, {
      props: { value: true },
      localVue,
      vuetify,
      store,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.find('section').exists()).toBe(true)
  })
})
