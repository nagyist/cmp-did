import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import { state, mutations, getters } from '~/store/authconfig.js'

import Register from '~/pages/Register.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

localVue.use(VueRouter)
const router = new VueRouter()

// Mock de localStorage
const localStorageMock = (function () {
  let store = {}

  return {
    getItem(key) {
      return store[key]
    },

    setItem(key, value) {
      store[key] = value
    },

    clear() {
      store = {}
    },

    removeItem(key) {
      delete store[key]
    },

    getAll() {
      // eslint-disable-next-line no-console
      console.log(store)
    },
  }
})()

Object.defineProperty(window, 'localStorage', { value: localStorageMock })

// eslint-disable-next-line import/no-named-as-default-member
const store = new Vuex.Store({
  modules: {
    authconfig: {
      namespaced: true,
      state,
      mutations,
      getters,
    },
  },
})

// Mock de firestore
const $fire = {
  firestore: {
    collection: jest.fn(() => {
      return {
        where: jest.fn(() => {
          return {
            get: jest.fn().mockReturnValue({ docs: [] }),
          }
        }),
        doc: jest.fn(() => {
          return {
            set: jest.fn(),
            get: jest.fn(() => {
              return {
                data: jest.fn().mockReturnValue({}),
              }
            }),
          }
        }),
      }
    }),
  },
  auth: {
    createUserWithEmailAndPassword: jest
      .fn()
      .mockReturnValue({ user: { uid: 'asdt86asd' } }),
  },
}

describe('Check Register functionalities', () => {
  let vuetify
  let wrapper

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
    window.localStorage.clear()
  })

  it('public_address computed returns the wallet of the user', async () => {
    wrapper = await shallowMount(Register, {
      localVue,
      vuetify,
      store,

      directives: {
        load() {},
      },

      mocks: {
        $t: (msg) => msg,
      },
    })
    await wrapper.setData({ wallet: '0a9s78dt7381jenm' })
    expect(wrapper.vm.public_address).toBe('0a9s78dt7381jenm')
  })

  it('validateSession doesnt let you register if you already have an account', () => {
    window.localStorage.setItem('cuit', '147652')
    window.localStorage.setItem('reino', 'r3in0')
    jest.useFakeTimers()
    wrapper = shallowMount(Register, {
      localVue,
      vuetify,
      store,

      directives: {
        load() {},
      },

      mocks: {
        $t: (msg) => msg,
        $fire,
      },
    })
    jest.advanceTimersByTime(2000)

    expect(wrapper.vm.cuit).toBe('147652')
    expect(wrapper.vm.reino).toBe('r3in0')
  })

  it('Clicking the "Back" button triggers the method clearData correctly', async () => {
    wrapper = await shallowMount(Register, {
      localVue,
      vuetify,
      store,

      computed: {
        loading() {
          return false
        },
      },

      directives: {
        load() {},
      },

      mocks: {
        $t: (msg) => msg,
        $fire,
      },

      data() {
        return {
          wallet: 'true',
          walletType: 'Coinbase',
          walletLink: {
            disconnect: jest.fn(),
          },
        }
      },
    })

    const backSpy = jest.spyOn(wrapper.vm, 'clearData')

    const backBtn = wrapper.find('#backBtn')
    backBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(backSpy).toBeCalledTimes(1)
    expect(wrapper.vm.wallet).toBe('')
    expect(wrapper.vm.walletType).toBe('')
  })

  it('Catch test when trying to submit the form', async () => {
    wrapper = await shallowMount(Register, {
      localVue,
      vuetify,
      store,

      directives: {
        load() {},
      },

      mocks: {
        $t: (msg) => msg,
      },

      data() {
        return {
          wallet: 'wall3t',
          data: {
            email: 'joker@mail.com',
            password: '*****',
            first_name: 'Ren',
            last_name: 'Nijima',
          },
          valid: true,
          isValid: true,
        }
      },
    })

    const backSpy = jest.spyOn(wrapper.vm, 'onSubmit')

    await wrapper.vm.onSubmit()

    expect(backSpy).toBeCalledTimes(1)
    expect(wrapper.vm.snackbar.show).toBe(true)
    expect(wrapper.vm.snackbar.text).toBe(
      "Cannot read property 'firestore' of undefined"
    )
    expect(wrapper.vm.snackbar.color).toBe('error')
  })

  it('Register is not succesful when the public adress is already in use', async () => {
    const $fire = {
      firestore: {
        collection: jest.fn(() => {
          return {
            where: jest.fn(() => {
              return {
                get: jest.fn().mockReturnValue({ docs: ['1321asdytas'] }),
              }
            }),
            doc: jest.fn(() => {
              return {
                get: jest.fn(() => {
                  return {
                    data: jest.fn().mockReturnValue({}),
                  }
                }),
              }
            }),
          }
        }),
      },
    }
    wrapper = await shallowMount(Register, {
      localVue,
      vuetify,
      store,

      directives: {
        load() {},
      },

      mocks: {
        $t: (msg) => msg,
        $fire,
      },

      data() {
        return {
          wallet: 'wall3t',
          data: {
            email: 'joker@mail.com',
            password: '*****',
            first_name: 'Ren',
            last_name: 'Nijima',
          },
          valid: true,
          isValid: true,
        }
      },
    })

    const backSpy = jest.spyOn(wrapper.vm, 'onSubmit')

    await wrapper.vm.onSubmit()

    expect(backSpy).toBeCalledTimes(1)
    expect(wrapper.vm.snackbar.show).toBe(true)
    expect(wrapper.vm.snackbar.text).toBe(
      'La llave pública ya está en uso por otra cuenta.'
    )
    expect(wrapper.vm.snackbar.color).toBe('error')
  })

  it('Register is succesful when no public address is found', async () => {
    router.push({ path: '/register' })
    wrapper = await shallowMount(Register, {
      localVue,
      vuetify,
      store,
      router,

      directives: {
        load() {},
      },

      mocks: {
        $t: (msg) => msg,
        $fire,
      },

      data() {
        return {
          wallet: 'wall3t',
          data: {
            email: 'joker@mail.com',
            password: '*****',
            first_name: 'Ren',
            last_name: 'Nijima',
          },
          valid: true,
          isValid: true,
        }
      },
    })

    jest.useFakeTimers()

    const backSpy = jest.spyOn(wrapper.vm, 'onSubmit')

    await wrapper.vm.onSubmit()

    expect(backSpy).toBeCalledTimes(1)
    expect(wrapper.vm.snackbar.show).toBe(true)
    expect(wrapper.vm.snackbar.text).toBe('Se registró el usuario exitosamente')
    expect(wrapper.vm.snackbar.color).toBe('success')

    jest.advanceTimersByTime(2100)

    expect(router.currentRoute.fullPath).toBe('/')
  })
})
