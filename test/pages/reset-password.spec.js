import { createLocalVue, mount } from '@vue/test-utils'
import Vuetify from 'vuetify'

import ResetPassword from '@/pages/reset-password.vue'

const localVue = createLocalVue()
let vuetify
let wrapper

const $fireModule = {
  auth: {
    RecaptchaVerifier: jest.fn(() => ({
      render: jest.fn(() => Promise.resolve(1111)),
    })),
  },
}

describe('Reset Password Functionalities', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  it('When the component is mounted the method $fireModule is called', () => {
    wrapper = mount(ResetPassword, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
        $fireModule,
      },
      data() {
        return {
          email: 'joker@hotmail.com',
        }
      },
    })
    expect($fireModule.auth.RecaptchaVerifier).toHaveBeenCalled()
    expect(wrapper.vm.email).toBe('joker@hotmail.com')
  })

  it('Check callback existence when mounting the component', () => {
    wrapper = mount(ResetPassword, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
        $fireModule,
      },
      data() {
        return {
          email: 'joker@hotmail.com',
        }
      },
    })
    const callbackFunction =
      $fireModule.auth.RecaptchaVerifier.mock.calls[0][1].callback()
    expect(callbackFunction).toBeDefined()
  })

  it('Clicking the submit button triggers the sendEmail method correctly', async () => {
    wrapper = mount(ResetPassword, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
        $fireModule,
      },
      data() {
        return {
          email: 'joker@mail.com',
          reCAPTCHAResetPassword: true,
        }
      },
    })

    const spyEmail = jest.spyOn(wrapper.vm, 'sendEmailVerificationSengrid')

    const sendBtn = wrapper.find('#sendEmailPassword')
    sendBtn.trigger('submit.prevent')
    await wrapper.vm.$nextTick()

    expect(spyEmail).toHaveBeenCalledTimes(1)
  })
})
