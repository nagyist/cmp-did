import Vuetify from 'vuetify'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import { mount, createLocalVue, RouterLinkStub } from '@vue/test-utils'

import Procedures from '@/pages/procedures/index.vue'
import { state, mutations, actions } from '@/store/procedures.js'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Testing index of Procedures', () => {
  let vuetify
  let wrapper

  // eslint-disable-next-line import/no-named-as-default-member
  const store = new Vuex.Store({
    state,
    mutations,
    actions,
    commit: {
      'procedures/SET_PROCEDURES': () => {},
    },
    getters: {
      'procedures/getActiveProcedures': () => {
        return [
          {
            link: 'clave-fiscal',
            long_description:
              'Consectetur exercitation consequat ad est sint qui id esse qui labore magna. Ad sunt occaecat in qui exercitation laborum sint incididunt deserunt et esse. Elit ipsum labore aute aliquip minim mollit.',
            short_description:
              'Ipsum ullamco ex esse laboris occaecat consequat id non et sit elit consequat do exercitation. Enim laborum Lorem officia officia sunt anim. Sunt laboris aute aliquip consectetur tempor sint excepteur. Cillum consequat eu sit nulla labore amet officia dolore minim occaecat. Officia voluptate laboris laboris et laborum tempor incididunt. Mollit cillum aliquip quis anim consequat mollit minim cillum nostrud fugiat dolor non fugiat sunt. Exercitation Lorem elit aute aliqua reprehenderit laborum minim excepteur eiusmod.',
            title: 'Certificado de Clave Fiscal',
            active: true,
          },
        ]
      },
    },
  })

  const router = new VueRouter()

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    wrapper.destroy()
  })
  test('should mount basic components succesfully', () => {
    wrapper = mount(Procedures, {
      localVue,
      vuetify,
      store,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.find('.hero-title').exists()).toBe(true)
    expect(wrapper.find('.hero-subtitle').exists()).toBe(true)
    expect(wrapper.find('.procedures-title').exists()).toBe(true)
  })

  test('should mount a procedure cards if a procedure is available', () => {
    const data = () => {
      return {
        procedures: [
          {
            link: 'clave-fiscal',
            long_description:
              'Consectetur exercitation consequat ad est sint qui id esse qui labore magna. Ad sunt occaecat in qui exercitation laborum sint incididunt deserunt et esse. Elit ipsum labore aute aliquip minim mollit.',
            short_description:
              'Ipsum ullamco ex esse laboris occaecat consequat id non et sit elit consequat do exercitation. Enim laborum Lorem officia officia sunt anim. Sunt laboris aute aliquip consectetur tempor sint excepteur. Cillum consequat eu sit nulla labore amet officia dolore minim occaecat. Officia voluptate laboris laboris et laborum tempor incididunt. Mollit cillum aliquip quis anim consequat mollit minim cillum nostrud fugiat dolor non fugiat sunt. Exercitation Lorem elit aute aliqua reprehenderit laborum minim excepteur eiusmod.',
            title: 'Certificado de Clave Fiscal',
            active: true,
          },
        ],
      }
    }
    const mocks = {
      $nuxt: {
        $route: {
          path: 'procedures',
        },
      },
    }
    wrapper = mount(Procedures, {
      localVue,
      vuetify,
      store,
      router,
      data,
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    // console.log(wrapper.vm.procedures)
    // console.log(wrapper.html())
    expect(wrapper.find('.v-card').exists()).toBe(true)
  })

  test('should not mount procedures cards if there is no procedure available', () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      state,
      mutations,
      actions,
      commit: {
        'procedures/SET_PROCEDURES': () => {},
      },
      getters: {
        'procedures/getActiveProcedures': () => {
          return []
        },
      },
    })
    const data = () => {
      return {
        procedures: [],
      }
    }
    const mocks = {
      $nuxt: {
        $route: {
          path: 'procedures',
        },
      },
    }
    wrapper = mount(Procedures, {
      localVue,
      vuetify,
      store,
      router,
      data,
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    // console.log(wrapper.vm.procedures)
    // console.log(wrapper.html())
    expect(wrapper.find('.v-card').exists()).toBe(false)
  })

  // test('should mount procedures cards if there are procedures in firestore', async () => {
  //   const data = () => {
  //     return {
  //       procedures: [],
  //     }
  //   }
  //   const mocks = {
  //     $fire: {
  //       firestore: {
  //         collection: jest.fn(() => {
  //           return {
  //             get: jest.fn().mockReturnValue({
  //               empty: false,
  //               docs: [
  //                 {
  //                   data: () => {
  //                     return {
  //                       active: true,
  //                       id: 'asdSDanjNBqwe238976NBg123nbasd73',
  //                       link: 'clave-fiscal',
  //                       long_description:
  //                         'Consectetur exercitation consequat ad est sint qui id esse qui labore magna. Ad sunt occaecat in qui exercitation laborum sint incididunt deserunt et esse. Elit ipsum labore aute aliquip minim mollit.',
  //                       short_description:
  //                         'Ipsum ullamco ex esse laboris occaecat consequat id non et sit elit consequat do exercitation. Enim laborum Lorem officia officia sunt anim. Sunt laboris aute aliquip consectetur tempor sint excepteur. Cillum consequat eu sit nulla labore amet officia dolore minim occaecat. Officia voluptate laboris laboris et laborum tempor incididunt. Mollit cillum aliquip quis anim consequat mollit minim cillum nostrud fugiat dolor non fugiat sunt. Exercitation Lorem elit aute aliqua reprehenderit laborum minim excepteur eiusmod.',
  //                       title: 'Certificado de Clave Fiscal',
  //                     }
  //                   },
  //                 },
  //               ],
  //             }),
  //           }
  //         }),
  //       },
  //     },
  //     $nuxt: {
  //       $route: {
  //         path: 'procedures',
  //       },
  //     },
  //   }
  //   const store = new Store({
  //     state: {},
  //     mutations: {
  //       'procedures/SET_PROCEDURES': (procedures) => {
  //         state.procedures = procedures
  //       },
  //     },
  //   })
  //   wrapper = await shallowMount(Procedures, {
  //     localVue,
  //     vuetify,
  //     store,
  //     router,
  //     data,
  //     mocks,
  //     stubs: {
  //       NuxtLink: RouterLinkStub,
  //     },
  //   })
  //   await wrapper.vm.$nextTick()
  //   expect(wrapper.find('procedurecard-stub').exists()).toBe(true)
  // })
  // test("shouldn't mount procedures cards if there aren't procedures in firestore", async () => {
  //   const data = () => {
  //     return {
  //       procedures: [],
  //     }
  //   }
  //   const mocks = {
  //     $fire: {
  //       firestore: {
  //         collection: jest.fn(() => {
  //           return {
  //             get: jest.fn().mockReturnValue({
  //               empty: true,
  //               docs: [],
  //             }),
  //           }
  //         }),
  //       },
  //     },
  //     $nuxt: {
  //       $route: {
  //         path: 'procedures',
  //       },
  //     },
  //   }
  //   const store = new Store({
  //     state: {},
  //     mutations: {
  //       'procedures/SET_PROCEDURES': (procedures) => {
  //         state.procedures = procedures
  //       },
  //     },
  //   })
  //   wrapper = await shallowMount(Procedures, {
  //     localVue,
  //     vuetify,
  //     store,
  //     router,
  //     data,
  //     mocks,
  //     stubs: {
  //       NuxtLink: RouterLinkStub,
  //     },
  //   })
  //   await wrapper.vm.$nextTick()
  //   expect(wrapper.find('procedurecard-stub').exists()).toBe(false)
  // })
})
