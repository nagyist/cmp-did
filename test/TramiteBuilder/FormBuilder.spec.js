import Vuex from 'vuex'
import { mount, createLocalVue } from '@vue/test-utils'

import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import FormBuilder from '~/components/forms/FormBuilder.vue'
import WithSections from '~/components/forms/WithSections.vue'
import NoSections from '~/components/forms/NoSections.vue'
import { state, mutations, getters, actions } from '~/store/notifications'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
document.body.setAttribute('data-app', true)

const flushPromises = () => new Promise((resolve) => setTimeout(resolve, 0))
const router = new VueRouter()

// eslint-disable-next-line import/no-named-as-default-member
const store = new Vuex.Store({
  modules: {
    notifications: {
      namespaced: true,
      state,
      mutations,
      getters,
      actions,
    },
    procedures: {
      state: {},
      actions: {
        'procedures/get': () => {
          return []
        },
      },
    },
  },
})

const $fire = {
  firestore: {
    collection: jest.fn(() => {
      return {
        doc: jest.fn(() => {
          return {
            collection: jest.fn(() => {
              return {
                add: jest.fn(),
              }
            }),
            set: jest.fn(() => Promise.resolve()),
          }
        }),
        get: jest.fn().mockImplementation(() =>
          Promise.resolve([
            {
              data: () => {
                return {
                  title: 'nombre',
                }
              },
            },
          ])
        ),
      }
    }),
  },
}

describe('Check Form Builder general functionalities', () => {
  let vuetify
  let wrapper

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
    wrapper.destroy()
  })

  it('NoSections is a Vue instance', () => {
    wrapper = mount(NoSections, {
      localVue,
      store,
      vuetify,
      router,

      mocks: {
        $t: (msg) => msg,
      },
    })
    expect(wrapper.vm).toBeTruthy()
  })

  it('WithSections is a Vue instance', () => {
    wrapper = mount(WithSections, {
      localVue,
      store,
      vuetify,
      router,

      mocks: {
        $t: (msg) => msg,
      },
    })
    expect(wrapper.vm).toBeTruthy()
  })

  it('Save method is called correctly when trying to save the form', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          activeProcedure: true,
          certificationType: 'manual',
          withSections: false,
          title: 'formulario',
          list: [
            {
              id: 'pregunta',
              kind: 'checkbox',
              label: 'pregunta',
              options: ['Opción 1', 'Opción 2'],
              type: 'text',
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const spySave = jest.spyOn(wrapper.vm, 'save')

    const saveButton = wrapper.find('#saveForm')
    saveButton.trigger('click')
    await wrapper.vm.$nextTick()

    await flushPromises()

    expect(spySave).toHaveBeenCalledTimes(1)
    expect(router.currentRoute.path).toBe('/admin/forms/list')
  })

  it('Save method catch error test', async () => {
    const $fire = {
      firestore: {
        collection: jest.fn(() => {
          return {
            doc: jest.fn(() => {
              return {
                collection: jest.fn(() => {
                  return {
                    add: jest.fn(),
                  }
                }),
              }
            }),
            get: jest.fn().mockImplementation(() =>
              Promise.resolve([
                {
                  data: () => {
                    return {
                      title: 'nombre',
                    }
                  },
                },
              ])
            ),
          }
        }),
      },
    }

    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          activeProcedure: true,
          certificationType: 'manual',
          withSections: false,
          title: 'formulario',
          list: [
            {
              id: 'pregunta',
              kind: 'checkbox',
              label: 'pregunta',
              options: ['Opción 1', 'Opción 2'],
              type: 'text',
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const spySave = jest.spyOn(wrapper.vm, 'save')

    const saveButton = wrapper.find('#saveForm')
    saveButton.trigger('click')
    await wrapper.vm.$nextTick()

    await flushPromises()

    expect(spySave).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.loading).toBe(false)
  })

  it('Changing between With Sections and No Sections cleans the list of questions', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
          title: 'formulario',
          list: [
            {
              id: 'pregunta',
              kind: 'checkbox',
              label: 'pregunta',
              options: ['Opción 1', 'Opción 2'],
              type: 'text',
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const sectionRadio = wrapper.find('#chOptSectionRadio')
    sectionRadio.setChecked()

    await wrapper.vm.changeOption()

    expect(wrapper.vm.list.length).toBe(0)
    expect(wrapper.vm.sections.length).toBe(0)
  })

  it('Initialize catch error test', async () => {
    const $fire = {
      firestore: {
        collection: jest.fn(() => {
          return {
            doc: jest.fn(() => {
              return {
                collection: jest.fn(() => {
                  return {
                    add: jest.fn(),
                  }
                }),
                set: jest.fn(() => {
                  return {
                    then: jest.fn(),
                  }
                }),
              }
            }),
            get: jest
              .fn()
              .mockImplementation(() =>
                Promise.reject(new Error('Something went wrong'))
              ),
          }
        }),
      },
    }

    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
          title: 'formulario',
          list: [
            {
              id: 'pregunta',
              kind: 'checkbox',
              label: 'pregunta',
              options: ['Opción 1', 'Opción 2'],
              type: 'text',
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const consoleErrorMock = jest.spyOn(console, 'error').mockImplementation()

    await flushPromises()

    expect(consoleErrorMock).toHaveBeenCalledTimes(1)
    consoleErrorMock.mockRestore()
  })

  it('Changing the name of a question changes its id', () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          title: 'formulario',
          withSections: false,
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'short',
              type: 'text',
              options: ['Opción 1'],
              defaultAnswer: true,
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.list[0].id).toBe('pregunta1')

    wrapper.vm.changeId({
      question: {
        label: 'Pregunta 2',
        id: '',
        kind: 'short',
        type: 'text',
        options: ['Opción 1'],
        defaultAnswer: true,
      },
      idx: 0,
    })

    expect(wrapper.vm.list[0].id).toBe('pregunta2')
  })

  it('Removing options from a question works correctly', () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          title: 'formulario',
          withSections: false,
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'radio',
              type: 'text',
              options: ['Opción 1', 'Opción 2'],
              defaultAnswer: true,
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.list[0].options.length).toBe(2)

    wrapper.vm.removeOption({
      question: wrapper.vm.list[0],
      i: 0,
    })

    expect(wrapper.vm.list[0].options.length).toBe(1)
  })

  it('Changing if you want to autocomplete the question triggers the resetDefault method', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
          title: 'formulario',
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'radio',
              type: 'text',
              options: ['Opción 1', 'Opción 2'],
              defaultAnswer: true,
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.list[0].defaultAnswer).toBeTruthy()

    await wrapper.vm.resetDefault(wrapper.vm.list[0])

    expect(wrapper.vm.list[0].defaultAnswer).toBe(null)
    expect(wrapper.vm.list[0].defAnsw).toBe(null)
  })

  it('Changing between manual and automatic certification works correctly', () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          title: 'formulario',
          withSections: true,
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.certificationType).toBe(null)

    const autoRadio = wrapper.find('#certTypeRadioAuto')
    autoRadio.setChecked()

    expect(wrapper.vm.certificationType).toBe('automatic')

    const manualRadio = wrapper.find('#certTypeRadioMan')
    manualRadio.setChecked()

    expect(wrapper.vm.certificationType).toBe('manual')
  })

  it('Marking the procedure as active marks the procedure with that flag', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          title: 'formulario',
          withSections: false,
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const oblSwitch = wrapper.find('#activeProcedureSwitch')
    oblSwitch.trigger('change')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.activeProcedure).toBeTruthy()

    oblSwitch.trigger('change')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.activeProcedure).toBeFalsy()
  })
})

describe('Check Form Builder functionality without sections', () => {
  let vuetify
  let wrapper

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
    wrapper.destroy()
  })

  it('Adding a question changes the list of questions', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
          list: [
            { id: 'nombre', kind: 'short' },
            { id: 'apellidos', kind: 'short' },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const addButton = wrapper.find('#addQNoSection')
    addButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list.length).toBe(3)
  })

  it('Remove questions correctly', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
          list: [
            { id: 'pregunta1', kind: 'short' },
            { id: 'pregunta2', kind: 'short' },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.list).toEqual(
      expect.arrayContaining([
        { id: 'pregunta1', kind: 'short' },
        { id: 'pregunta2', kind: 'short' },
      ])
    )

    const rmvQButton = wrapper.find('#rmvQNoSection')
    rmvQButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list.length).toBe(1)
    expect(wrapper.vm.list).toEqual(
      expect.arrayContaining([{ id: 'pregunta2', kind: 'short' }])
    )
  })

  it('In case of checkbox/dropdown/radio you can add options correctly', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
          list: [
            {
              id: 'pregunta',
              kind: 'checkbox',
              label: 'pregunta',
              options: ['Opción 1', 'Opción 2'],
              type: 'text',
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.list[0].options).toEqual(['Opción 1', 'Opción 2'])

    const addOptionButton = wrapper.find('#addOptNoSections')
    addOptionButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].options).toEqual([
      'Opción 1',
      'Opción 2',
      'Opción 3',
    ])
  })

  it('Marking the question as obligatory marks the question with that flag', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const addButton = wrapper.find('#addQNoSection')
    addButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list.length).toBe(1)

    const oblSwitch = wrapper.find('#obligatoryNoSections')
    oblSwitch.trigger('change')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].required).toBeTruthy()
  })

  it('Marking the question with spacer marks the question with that flag', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const addButton = wrapper.find('#addQNoSection')
    addButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list.length).toBe(1)

    const spacerSwitch = wrapper.find('#spacerNoSections')
    spacerSwitch.trigger('change')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].spacer).toBeTruthy()
  })

  it('Marking the question with certify marks the question with that flag', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const addButton = wrapper.find('#addQNoSection')
    addButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list.length).toBe(1)

    const certifySwitch = wrapper.find('#certifyNoSections')
    certifySwitch.trigger('change')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].certify).toBeTruthy()
  })

  it('Selecting a default answer in a question marks that flag in the question', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'short',
              type: 'text',
              options: ['Opción 1'],
              defaultAnswer: true,
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.list.length).toBe(1)

    const dropdownAnsw = wrapper.find('#dropdownAnswers').find('.v-select')
    expect(dropdownAnsw.props('items').length).toBe(4)

    dropdownAnsw.vm.selectItem({ id: 'email', name: 'Correo electronico' })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].defAnsw).toBe('email')
  })

  it('Marking the question as disabled marks the question with that flag', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: false,
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'short',
              type: 'text',
              options: ['Opción 1'],
              defaultAnswer: true,
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.list.length).toBe(1)

    const disableSwitch = wrapper.find('#disabledNoSections')
    disableSwitch.trigger('change')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].disabled).toBeTruthy()
  })
})

describe('Check Form Builder functionality with sections', () => {
  let vuetify
  let wrapper

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
    wrapper.destroy()
  })

  it('Add sections correctly', () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const sectionNameField = wrapper.find('#sectionField')
    sectionNameField.setValue('Section A')

    const sectionRadio = wrapper.find('#sectionRadio')
    sectionRadio.setChecked()

    wrapper.vm.addSection()

    expect(wrapper.vm.sections.length).toBe(1)
    expect(wrapper.vm.sections).toEqual(
      expect.arrayContaining([
        {
          name: 'Section A',
          id: 'sectionA',
          multipleAnswers: true,
        },
      ])
    )
  })

  it('Delete sections correctly', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          deleteIndex: 0,
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.sections.length).toBe(1)
    expect(wrapper.vm.sections).toEqual(
      expect.arrayContaining([
        {
          name: 'Section 1',
          id: 'section1',
          multipleAnswers: true,
        },
      ])
    )

    const deleteSectionBtn = wrapper.find('#deleteSection')
    deleteSectionBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.sections.length).toBe(0)
  })

  it('Adding a question changes the list of questions', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          list: [
            { id: 'nombre', kind: 'short' },
            { id: 'apellidos', kind: 'short' },
          ],
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
          ],
          selectedSection: 'section1',
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const addButton = wrapper.find('#addQWSection')
    addButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list.length).toBe(3)
  })

  it('Remove questions correctly', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
          ],
          selectedSection: 'section1',
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'short',
              type: 'text',
              options: ['Opción 1'],
              section: 'section1',
              originalIndex: 0,
            },
            {
              label: 'Pregunta 2',
              id: 'pregunta2',
              kind: 'short',
              type: 'text',
              options: ['Opción 1'],
              section: 'section1',
              originalIndex: 1,
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.list).toEqual(
      expect.arrayContaining([
        {
          label: 'Pregunta 1',
          id: 'pregunta1',
          kind: 'short',
          type: 'text',
          options: ['Opción 1'],
          section: 'section1',
          originalIndex: 0,
        },
        {
          label: 'Pregunta 2',
          id: 'pregunta2',
          kind: 'short',
          type: 'text',
          options: ['Opción 1'],
          section: 'section1',
          originalIndex: 1,
        },
      ])
    )

    await wrapper.vm.removeAt(0)

    expect(wrapper.vm.list.length).toBe(1)
    expect(wrapper.vm.list).toEqual(
      expect.arrayContaining([
        {
          label: 'Pregunta 2',
          id: 'pregunta2',
          kind: 'short',
          type: 'text',
          options: ['Opción 1'],
          section: 'section1',
          originalIndex: 0,
        },
      ])
    )
  })

  it('In case of checkbox/dropdown/radio you can add options correctly', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
          ],
          selectedSection: 'section1',
          list: [
            {
              id: 'pregunta',
              kind: 'checkbox',
              label: 'pregunta',
              options: ['Opción 1', 'Opción 2'],
              type: 'text',
              section: 'section1',
              originalIndex: 0,
            },
          ],
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.list[0].options).toEqual(['Opción 1', 'Opción 2'])

    const addOptionButton = wrapper.find('#addOptWSections')
    addOptionButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].options).toEqual([
      'Opción 1',
      'Opción 2',
      'Opción 3',
    ])
  })

  it('Marking the question as obligatory marks the question with that flag', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
          ],
          selectedSection: 'section1',
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const addButton = wrapper.find('#addQWSection')
    addButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list.length).toBe(1)

    const oblSwitch = wrapper.find('#obligatoryWSections')
    oblSwitch.trigger('change')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].required).toBeTruthy()
  })

  it('Marking the question with spacer marks the question with that flag', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
          ],
          selectedSection: 'section1',
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const addButton = wrapper.find('#addQWSection')
    addButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list.length).toBe(1)

    const spacerSwitch = wrapper.find('#spacerWSections')
    spacerSwitch.trigger('change')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].spacer).toBeTruthy()
  })

  it('Marking the question with certify marks the question with that flag', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
          ],
          selectedSection: 'section1',
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const addButton = wrapper.find('#addQWSection')
    addButton.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list.length).toBe(1)

    const certifySwitch = wrapper.find('#certifyWSections')
    certifySwitch.trigger('change')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].certify).toBeTruthy()
  })

  it('Marking the question as disabled marks the question with that flag', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
          ],
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'short',
              type: 'text',
              options: ['Opción 1'],
              section: 'section1',
              originalIndex: 0,
              defaultAnswer: true,
            },
          ],
          selectedSection: 'section1',
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    const disableSwitch = wrapper.find('#disabledWSections')
    disableSwitch.trigger('change')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].disabled).toBeTruthy()
  })

  it('Selecting a default answer in a question marks that flag in the question', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
          ],
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'short',
              type: 'text',
              options: ['Opción 1'],
              section: 'section1',
              originalIndex: 0,
              defaultAnswer: true,
            },
          ],
          selectedSection: 'section1',
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.list.length).toBe(1)

    const dropdownAnsw = wrapper.find('#dropdownAnswers').find('.v-select')
    expect(dropdownAnsw.props('items').length).toBe(4)

    dropdownAnsw.vm.selectItem({ id: 'email', name: 'Correo electronico' })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.list[0].defAnsw).toBe('email')
  })

  it('When editing a section, if the name is acceptable, the computed property validEditSection returns true', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
          ],
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'short',
              type: 'text',
              options: ['Opción 1'],
              section: 'section1',
              originalIndex: 0,
              defaultAnswer: true,
            },
          ],
          selectedSection: 'section1',
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.sections.length).toBe(1)
    expect(wrapper.vm.sections).toEqual(
      expect.arrayContaining([
        {
          name: 'Section 1',
          id: 'section1',
          multipleAnswers: true,
        },
      ])
    )

    const deleteSectionBtn = wrapper.find('#editSection')
    deleteSectionBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.editName).toBe('Section 1')
    expect(wrapper.vm.editIndex).toBe(0)
    expect(wrapper.vm.editMultiple).toBeTruthy()

    wrapper.setData({ editName: 'Section 2' })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.validEditSection).toBe(true)
  })

  it('When editing a section, if the name is not acceptable, the computed property validEditSection returns false', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
            { name: 'Section 2', id: 'section2', multipleAnswers: true },
          ],
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'short',
              type: 'text',
              options: ['Opción 1'],
              section: 'section1',
              originalIndex: 0,
              defaultAnswer: true,
            },
          ],
          selectedSection: 'section1',
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.sections.length).toBe(2)

    const editSectionBtn = wrapper.find('#editSection')
    editSectionBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.editName).toBe('Section 1')
    expect(wrapper.vm.editIndex).toBe(0)
    expect(wrapper.vm.editMultiple).toBeTruthy()

    wrapper.setData({ editName: 'Section 2' })
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.validEditSection).toBe(false)
  })

  it('When editing a section, the sections is updated with the new values', async () => {
    wrapper = mount(FormBuilder, {
      localVue,
      store,
      vuetify,
      router,

      data() {
        return {
          withSections: true,
          sections: [
            { name: 'Section 1', id: 'section1', multipleAnswers: true },
            { name: 'Section 2', id: 'section2', multipleAnswers: true },
          ],
          list: [
            {
              label: 'Pregunta 1',
              id: 'pregunta1',
              kind: 'short',
              type: 'text',
              options: ['Opción 1'],
              section: 'section1',
              originalIndex: 0,
              defaultAnswer: true,
            },
          ],
          selectedSection: 'section1',
        }
      },

      mocks: {
        $fire,
        $t: (msg) => msg,
      },
      stubs: ['router-link'],
    })

    expect(wrapper.vm.sections.length).toBe(2)

    const editSectionBtn = wrapper.find('#editSection')
    editSectionBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.editName).toBe('Section 1')
    expect(wrapper.vm.editIndex).toBe(0)
    expect(wrapper.vm.editMultiple).toBeTruthy()

    wrapper.setData({ editName: 'Section 3', editMultiple: false })
    await wrapper.vm.$nextTick()

    const saveEditBtn = wrapper.find('#saveEdit')
    saveEditBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.sections.length).toEqual(2)
    expect(wrapper.vm.sections).toEqual(
      expect.arrayContaining([
        {
          name: 'Section 3',
          id: 'section3',
          multipleAnswers: false,
        },
      ])
    )
  })
})
