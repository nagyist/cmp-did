import { createLocalVue, mount } from '@vue/test-utils'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import axios from 'axios'

import DataTableCertificates from '@/components/Certificates/DataTableCertificates.vue'

const localVue = createLocalVue()
localVue.use(VueRouter)
localVue.use(Vuex)

let vuetify

document.body.setAttribute('data-app', true)

// eslint-disable-next-line import/no-named-as-default-member
const $store = new Vuex.Store({
  state: { currentUrl: 'currenturl.xs' },
  getters: {
    'authconfig/autenticar': () => {
      return {
        autenticar: true,
        email: false,
        mobile: false,
        wallet: false,
        register: true,
        did: true,
      }
    },
  },
})

jest.mock('axios')

const certificates = {
  ref: 'certificates/fda788uhJN8A08dh',
  name: 'Fondo Platense',
  email: 'queen@mail.com',
  date: '2021-11-02',
  id_reference: 'fda788uhJN8A08dh',
  cuit: '454545',
  link: 'https://certs.fondo.platense.gob.ar/90cf53ee-4b06-4bc7-8f20-b90f5853a1a2',
  plazo_total: '45',
  is_certified: true,
  address: 'Domicilio Test',
  pagoPrimeraCuota: '2021-12-24',
  created: '2021-11-08T12:27:53-06:00',
  pdf: 'lorem-ipsum.pdf',
  certify: true,
  estado_origen: 'Certificado',
  firstName: 'Test',
  is_revocated: false,
  dni: '45454545',
  dateFinPG: '2021-11-24',
  locality: 'Mexico',
  identity: 'info@mail.gob.ar',
  pubkey: 'ecdsa-koblitz-pubkey:gsfg4987Yuhbjaskioa987',
  lastName: 'test',
}

const toCertify = {
  ref: 'certificates/asd796gfbhas2',
  cuit: '1234',
  date: '2021-06-01',
  email: 'joker@email.com',
  name: 'Fondo Platense',
  created: '2021-11-11T14:03:23-07:00',
  certify: false,
  lastName: 'Navarro',
  firstName: 'Jose',
  address: 'Dom',
  identity: 'info@mail.gob.ar',
  pdf: 'lorem-ipsum.pdf',
  is_certified: false,
  pubkey: 'ecdsa-koblitz-pubkey:0xdd62dC2c3e842Ee0cb6Ac99519c3571Cd91137E1',
  locality: 'Loc',
  dni: '3124',
  is_revocated: false,
}

const revokedCertificates = {
  ref: 'certificates/asd796gfbhas2',
  cuit: '1234',
  date: '2021-06-01',
  email: 'joker@email.com',
  name: 'Fondo Platense',
  created: '2021-11-11T14:03:23-07:00',
  certify: false,
  lastName: 'Navarro',
  firstName: 'Jose',
  address: 'Dom',
  identity: 'info@mail.gob.ar',
  pdf: 'lorem-ipsum.pdf',
  is_certified: false,
  pubkey: 'ecdsa-koblitz-pubkey:0xdd62dC2c3e842Ee0cb6Ac99519c3571Cd91137E1',
  locality: 'Loc',
  dni: '3124',
  is_revocated: true,
}

const headersProps = [
  { text: 'Dni', value: 'dni', sortable: true, width: 170 },
  { text: 'Date', value: 'date', sortable: true, width: 170 },
  { text: 'Locality', value: 'locality', sortable: true, width: 170 },
  { text: 'Cuit', value: 'cuit', sortable: true, width: 170 },
  {
    text: 'FirstName',
    value: 'firstName',
    sortable: true,
    width: 170,
  },
  { text: 'Address', value: 'address', sortable: true, width: 170 },
  { text: 'LastName', value: 'lastName', sortable: true, width: 170 },
  { text: 'Email', value: 'email', sortable: true, width: 170 },
]

const activityObj = {
  ref: 'certificates/asda9787ycADSF7',
  link: 'https://ibd.certs.oro.gob.ar/a2eaa75e-f3r4-4597-234-f41dc58bcsda',
  domicilios: ['234 24 CP: 0, Holanda, Riverdale, No informada, Antartida'],
  created: '2021-11-10T07:21:10-03:00',
  title: 'Certificado para certificad',
  pubkey: 'ecdsa-koblitz-pubkey:0x8D605C63b83942C0cD3DcsdgfFSDa1940c981423D',
  is_certified: true,
  name_citizen: 'Ann Montoya',
  id_user: '63984173',
  id_reference: 'asda9787ycADSF7',
  actividad_principal: 'Actividad principal, con fecha de inicio el 01/02/2019',
  estado_origen: 'Certificado',
  type: 'certificado',
  public_address: '0x8D605C63sdffq3423D',
  n_cuit: '63984173',
  name: 'Ann Montoya',
  identity: 'info@mail.gov',
  actividades_secundarias: [
    { actividad: 'Cultivo de maíz', fecha_inicio: '01/04/2021' },
  ],
  comprobante: '63984173.pdf',
}

const activityHeaders = [
  { text: 'N cuit', value: 'n_cuit', sortable: true, width: 250 },
  {
    text: 'Name citizen',
    value: 'name_citizen',
    sortable: true,
    width: 250,
  },
  {
    text: 'Public address',
    value: 'public_address',
    sortable: true,
    width: 250,
  },
  { text: 'Link', value: 'link', sortable: true, width: 250 },
  {
    text: 'Actividad principal',
    value: 'actividad_principal',
    sortable: true,
    width: 250,
  },
  {
    text: 'Actividades secundarias',
    value: 'actividades_secundarias',
    sortable: true,
    width: 250,
  },
  {
    text: 'Domicilios',
    value: 'domicilios',
    sortable: true,
    width: 250,
  },
  {
    text: 'Comprobante',
    value: 'comprobante',
    sortable: true,
    width: 250,
  },
  { text: 'Created', value: 'created', sortable: true, width: 250 },
  { text: 'Title', value: 'title', sortable: true, width: 250 },
]

const flushPromises = () => new Promise((resolve) => setTimeout(resolve, 0))

describe('DataTableCertificates general functionalities tests', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('When the selected value changes, the watcher emits correctly the new value that is changed', () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        headers: headersProps,
        items: [toCertify],
      },
    })

    wrapper.setData({
      selected: [toCertify],
    })

    wrapper.vm.$options.watch.selected.call(wrapper.vm, wrapper.vm.selected)

    expect(wrapper.emitted().change).toBeTruthy()
    expect(wrapper.emitted().change[0][0][0]).toStrictEqual(toCertify)
  })

  it('Clicking in the "Ver archivo" button, emits the correct item', async () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: activityHeaders,
        items: [activityObj],
        deleteActivate: true,
        revalidateActivate: true,
      },
      data() {
        return {
          isFCMisiones: false,
        }
      },
    })

    const showFileBtn = wrapper.find('#showFile')
    showFileBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted().showFile).toBeTruthy()
    expect(wrapper.emitted().showFile[0][0]).toStrictEqual(activityObj)
  })

  it('Clicking in the "Ver Actividades" button, emits the correct item', async () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: activityHeaders,
        items: [activityObj],
        deleteActivate: true,
        revalidateActivate: true,
      },
      data() {
        return {
          isFCMisiones: false,
        }
      },
    })

    const showSecondaryActivitiesBtn = wrapper.find('#showSecondaryActivities')
    showSecondaryActivitiesBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted().showSecondaryActivities).toBeTruthy()
    expect(wrapper.emitted().showSecondaryActivities[0][0]).toStrictEqual([
      { actividad: 'Cultivo de maíz', fecha_inicio: '01/04/2021' },
    ])
  })

  it('Clicking in the "Ver domicilios" button, emits the correct item', async () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: activityHeaders,
        items: [activityObj],
        deleteActivate: true,
        revalidateActivate: true,
      },
      data() {
        return {
          isFCMisiones: false,
        }
      },
    })

    const showAddressBtn = wrapper.find('#showAddress')
    showAddressBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.emitted().showAddress).toBeTruthy()
    expect(wrapper.emitted().showAddress[0][0]).toStrictEqual([
      '234 24 CP: 0, Holanda, Riverdale, No informada, Antartida',
    ])
  })

  it('toggleAll method cleans the selected entries if some are selected', () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [toCertify],
        deleteActivate: true,
        revalidateActivate: true,
      },
      data() {
        return {
          isFCMisiones: false,
          selected: [toCertify],
        }
      },
    })

    expect(wrapper.vm.selected).toStrictEqual([toCertify])

    wrapper.vm.toggleAll()

    expect(wrapper.vm.selected).toStrictEqual([])
  })

  it('toggleAll method returns certified items when there are no selected items', () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [certificates],
        deleteActivate: true,
        revalidateActivate: true,
      },
      data() {
        return {
          isFCMisiones: false,
          selected: [],
        }
      },
    })

    expect(wrapper.vm.selected).toStrictEqual([])

    wrapper.vm.toggleAll()

    expect(wrapper.vm.selected).toStrictEqual([certificates])
  })

  it('toggleAll method returns al items when no option is selected in method', () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [],
        deleteActivate: true,
        revalidateActivate: true,
      },
      data() {
        return {
          isFCMisiones: false,
          selected: [],
        }
      },
    })

    wrapper.vm.toggleAll()

    expect(wrapper.vm.selected).toStrictEqual([])
  })
})

describe('DataTableCertificates tests for Not Certified Tab', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('The Delete entry header exists in the Not Certified Tab', () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [toCertify],
        deleteActivate: true,
      },
      data() {
        return {
          isFCMisiones: false,
        }
      },
    })

    expect(wrapper.vm.headersDeleteActivate).toEqual(
      expect.arrayContaining([
        {
          text: '',
          sortable: false,
          value: 'delete',
          align: 'center',
          width: 50,
        },
      ])
    )
  })

  it('When withAuthorized is true, the Send Email header exists in the Not Certified Tab', () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [toCertify],
        deleteActivate: true,
        withAuthorized: true,
      },
      data() {
        return {
          isFCMisiones: false,
        }
      },
    })

    expect(wrapper.vm.headersDeleteActivate).toEqual(
      expect.arrayContaining([
        {
          text: 'Send',
          sortable: false,
          value: 'send',
          align: 'center',
          width: 50,
        },
      ])
    )
  })

  it('sending email for authorization calls the axios.post correctly', async () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
        $store,
        axios,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [toCertify],
        deleteActivate: true,
        withAuthorized: true,
      },
      data() {
        return {
          isFCMisiones: false,
          urlEndpoints: 'http:link',
        }
      },
    })

    // Mock the response of axios to return a resolved promise
    axios.post.mockImplementation(() =>
      Promise.resolve({ status: 200, data: 'hjytdr78iJAHSBGd' })
    )

    const sendIndividualEmailBtn = wrapper.find('#sendIndividualEmail')
    sendIndividualEmailBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(axios.post).toHaveBeenCalledWith('http:link/createTokenUnicef', {
      email: 'joker@email.com',
      currentUrl: 'currenturl.xs',
    })

    await flushPromises()

    expect(axios.post).toHaveBeenCalledWith('http:link/sendEmail', {
      personalizations: [
        {
          dynamic_template_data: {
            button: 'continue',
            content: 'Visit this link to complete the process',
            link: 'hjytdr78iJAHSBGd',
            subject: 'Verification Email - UNICEF',
            title:
              'To receive your certificate on Blockchain, it must be authorized ',
            user: 'joker@email.com',
          },
          subject: 'Verification Email - UNICEF',
          to: [
            {
              email: 'joker@email.com',
            },
          ],
        },
      ],
    })
  })

  it('Test for catch clause when sending the email for authorization', async () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
        $store,
        axios,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [toCertify],
        deleteActivate: true,
        withAuthorized: true,
      },
      data() {
        return {
          isFCMisiones: false,
          urlEndpoints: 'http:link',
        }
      },
    })

    // Mock console error to spy on it. It also omits the errors in console
    const consoleErrorMock = jest.spyOn(console, 'error').mockImplementation()

    // Mock the response of axios to return a resolved promise
    axios.post.mockImplementation(() =>
      Promise.reject(new Error('Something went wrong'))
    )

    const sendIndividualEmailBtn = wrapper.find('#sendIndividualEmail')
    sendIndividualEmailBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(axios.post).toHaveBeenCalledWith('http:link/createTokenUnicef', {
      email: 'joker@email.com',
      currentUrl: 'currenturl.xs',
    })

    await flushPromises()

    expect(axios.post).not.toHaveBeenCalledWith('http:link/sendEmail', {
      personalizations: [
        {
          dynamic_template_data: {
            button: 'continue',
            content: 'Visit this link to complete the process',
            link: 'jhays7d6a5sdtg',
            subject: 'Verification Email - UNICEF',
            title:
              'To receive your certificate on Blockchain, it must be authorized ',
            user: 'joker@email.com',
          },
          subject: 'Verification Email - UNICEF',
          to: [
            {
              email: 'joker@email.com',
            },
          ],
        },
      ],
    })

    expect(consoleErrorMock).toHaveBeenCalledTimes(1)

    // Remove the mock from the console for future tests
    consoleErrorMock.mockRestore()
  })

  it('The Delete option is present and executes the dialog for confirmation', async () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [toCertify],
        deleteActivate: true,
      },
      data() {
        return {
          isFCMisiones: false,
        }
      },
    })

    const spyDelete = jest.spyOn(wrapper.vm, 'dialogConfirmDelete')

    const deleteBtn = wrapper.find('#deleteDialog')
    deleteBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.item).toStrictEqual(toCertify)
    expect(spyDelete).toHaveBeenCalledTimes(1)

    // wrapper.vm.archiveItem()

    // expect(wrapper.emitted().archiveItem).toBeTruthy()
    // expect(wrapper.emitted().archiveItem[0][0]).toStrictEqual(toCertify)
  })

  it('The deleteItem method emits correctly the item to delete', async () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [toCertify],
        deleteActivate: true,
      },
      data() {
        return {
          isFCMisiones: false,
        }
      },
    })

    const spyDelete = jest.spyOn(wrapper.vm, 'dialogConfirmDelete')

    const deleteBtn = wrapper.find('#deleteDialog')
    deleteBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.item).toStrictEqual(toCertify)
    expect(spyDelete).toHaveBeenCalledTimes(1)

    wrapper.vm.deleteItem()

    expect(wrapper.emitted().deleteItem).toBeTruthy()
    expect(wrapper.emitted().deleteItem[0][0]).toStrictEqual(toCertify)
  })
})

describe('DataTableCertificates tests for Certified Tab', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('The QR, Status, Revoke and Link headers are pushed to headersCertified in the Certified Tab', () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [certificates],
      },
    })

    wrapper.setData({
      selected: [certificates],
    })

    const expectedCertifiedHeaders = [
      {
        text: '',
        sortable: false,
        value: 'revoke',
        align: 'center',
        width: 80,
      },
      {
        text: 'status',
        sortable: true,
        value: 'estado_origen',
        align: 'center',
        width: 150,
      },
      {
        text: 'URL',
        sortable: true,
        value: 'link',
        align: 'center',
        width: 170,
      },
      {
        text: 'QR',
        sortable: true,
        value: 'qr',
        align: 'center',
        width: 150,
      },
    ]

    expect(wrapper.vm.headersCertified).toEqual(
      expect.arrayContaining(expectedCertifiedHeaders)
    )
  })

  it('For FCM, only the headers for "dni", "firstName", "lastName" are the only present', () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [certificates],
      },
      data() {
        return {
          isFCMisiones: true,
        }
      },
    })

    wrapper.setData({
      selected: [certificates],
    })

    const expectedCertifiedHeaders = [
      { text: 'LastName', value: 'lastName', sortable: true, width: 170 },
      { text: 'Dni', value: 'dni', sortable: true, width: 170 },
      { text: 'FirstName', value: 'firstName', sortable: true, width: 170 },
    ]

    const notExpectedCertifiedHeaders = [
      { text: 'Date', value: 'date', sortable: true, width: 170 },
      { text: 'Email', value: 'email', sortable: true, width: 170 },
      { text: 'Address', value: 'address', sortable: true, width: 170 },
      { text: 'Locality', value: 'locality', sortable: true, width: 170 },
    ]

    expect(wrapper.vm.headersCertified).toEqual(
      expect.arrayContaining(expectedCertifiedHeaders)
    )
    expect(wrapper.vm.headersCertified).toEqual(
      expect.not.arrayContaining(notExpectedCertifiedHeaders)
    )
  })

  it('The Revoke option is present and executes the dialog for confirmation', async () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [certificates],
      },
      data() {
        return {
          isFCMisiones: true,
        }
      },
    })

    const spyRevoke = jest.spyOn(wrapper.vm, 'dialogConfirm')

    const revokeBtn = wrapper.find('#activeDialog')
    revokeBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.item).toStrictEqual(certificates)
    expect(spyRevoke).toHaveBeenCalledTimes(1)
  })

  it('The archiveItem method emits correctly the item to archive', async () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [certificates],
      },
      data() {
        return {
          isFCMisiones: true,
        }
      },
    })

    const spyRevoke = jest.spyOn(wrapper.vm, 'dialogConfirm')

    const revokeBtn = wrapper.find('#activeDialog')
    revokeBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(wrapper.vm.item).toStrictEqual(certificates)
    expect(spyRevoke).toHaveBeenCalledTimes(1)

    wrapper.vm.archiveItem()

    expect(wrapper.emitted().archiveItem).toBeTruthy()
    expect(wrapper.emitted().archiveItem[0][0]).toStrictEqual(certificates)
  })
})

describe('DataTableCertificates tests for Revoked Tab', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('The QR and URL header exists in the Revoked Tab', () => {
    const wrapper = mount(DataTableCertificates, {
      localVue,
      vuetify,
      mocks: {
        $t: (msg) => msg,
      },
      propsData: {
        archiveActivate: true,
        headers: headersProps,
        items: [revokedCertificates],
        deleteActivate: true,
        revalidateActivate: true,
      },
      data() {
        return {
          isFCMisiones: false,
        }
      },
    })

    expect(wrapper.vm.headersRevoke).toEqual(
      expect.arrayContaining([
        {
          text: 'URL',
          sortable: true,
          value: 'link',
          align: 'center',
          width: 170,
        },
        {
          text: 'QR',
          sortable: true,
          value: 'qr',
          align: 'center',
          width: 150,
        },
      ])
    )
  })
})
