import Vuex from 'vuex'
import { mount, createLocalVue } from '@vue/test-utils'

import Vuetify from 'vuetify'
import MisionesBVCerts from '~/components/Certificates/MisionesBonosVerdesCerts.vue'
import { state, mutations, getters } from '~/store/certsConfig.js'

const localVue = createLocalVue()
localVue.use(Vuex)

const $i18n = {
  locale: 'es',
}

const $fire = {
  firestore: {
    collection: jest.fn(() => {
      return {
        add: jest.fn(),
      }
    }),
  },
}

const $fireModule = {
  storage: jest.fn(() => {
    return {
      ref: jest.fn(() => {
        return {
          child: jest.fn(() => {
            return {
              put: jest.fn(),
            }
          }),
        }
      }),
    }
  }),
}

// eslint-disable-next-line import/no-named-as-default-member
const store = new Vuex.Store({
  modules: {
    certsConfig: {
      namespaced: true,
      state,
      mutations,
      getters,
    },
  },
})

const flushPromises = () => new Promise((resolve) => setTimeout(resolve, 0))

describe('Check MisionesBVCerts functionalities', () => {
  let vuetify

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('Clicking the Cancel button emits to close the dialog', async () => {
    const FilePondStub = {
      render: () => {},
      methods: {
        removeFiles: () => true,
      },
    }

    const wrapper = mount(MisionesBVCerts, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: FilePondStub },
    })

    const spyCancel = jest.spyOn(wrapper.vm, 'closeDialog')
    const spyClear = jest.spyOn(wrapper.vm, 'clearData')

    const cancelBtn = wrapper.find('#cancelBtn')
    cancelBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(spyCancel).toHaveBeenCalledTimes(1)
    expect(spyClear).toHaveBeenCalledTimes(1)
    expect(wrapper.emitted().closeDialog).toBeTruthy()
  })

  it('The updateFiles method asigns the name correctly from the file uploaded from the FilePond component', async () => {
    const wrapper = mount(MisionesBVCerts, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyUpdateFiles = jest.spyOn(wrapper.vm, 'updateFiles')

    const filesMock = [{ file: { name: 'fileName' } }]

    await wrapper.vm.updateFiles(filesMock)

    expect(spyUpdateFiles).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.files).toStrictEqual(filesMock)
    expect(wrapper.vm.certificado.pdf).toBe('fileName')
  })

  it('The updateFiles method asigns empty filename if no name is found', async () => {
    const wrapper = mount(MisionesBVCerts, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyUpdateFiles = jest.spyOn(wrapper.vm, 'updateFiles')

    const filesMock = []

    await wrapper.vm.updateFiles(filesMock)

    expect(spyUpdateFiles).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.files).toStrictEqual(filesMock)
    expect(wrapper.vm.certificado.pdf).toBe('')
  })

  it('The errorUpdateFiles method, valid file size and asigns false to valid form', async () => {
    const wrapper = mount(MisionesBVCerts, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })
    const spyErrorUpdateFiles = jest.spyOn(wrapper.vm, 'errorUpdateFiles')
    const filesMock = [{ file: { size: 15554049 } }]

    await wrapper.vm.errorUpdateFiles(filesMock)

    expect(spyErrorUpdateFiles).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.valid).toBeFalsy()
  })

  it('Check that the save method works correctly and emits the correct message', async () => {
    const wrapper = mount(MisionesBVCerts, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
        $fireModule,
      },

      stubs: { 'client-only': true, FilePond: true },

      data() {
        return {
          certificado: {
            code: '46290',
            correlative: '223',
            year: '1999',
            body: '2',
            technical_report: '2000',
            technical_report_year: '2001',
            disposition: '456',
            date: '2022-01-04',
            office_department: '22',
            municipality: 'Municipio',
            section: '22',
            chacra: '1234',
            apple: '1234',
            parcela: '3456',
            initiator_name: 'John Breakfast',
            initiator_dni: '234562241',
            kind_of_person: 'Física',
            applicant_name: 'Ricardo Dinner',
            applicant_dni: '53455445',
            applicant_caracter: 'Rare',
            applicant_destination: 'Rarto',
            pdf: 'fileName',
          },
          files: [{ file: { name: 'fileName' } }],
        }
      },
    })

    const spySave = jest.spyOn(wrapper.vm, 'save')

    const cancelBtn = wrapper.find('#saveBtn')
    cancelBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(spySave).toHaveBeenCalledTimes(1)

    await flushPromises()

    expect(wrapper.emitted().save).toBeTruthy()
    expect(wrapper.emitted().messages).toBeTruthy()
    expect(wrapper.emitted().messages[0][0].error).toBeFalsy()
    expect(wrapper.emitted().messages[0][0].message).toBe(
      'Los datos fueron guardados.'
    )
  })

  it('Catch test for save method works correctly and emits the correct message', async () => {
    const wrapper = mount(MisionesBVCerts, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fireModule,
      },

      stubs: { 'client-only': true, FilePond: true },

      data() {
        return {
          certificado: {
            code: '46290',
            correlative: '223',
            year: '1999',
            body: '2',
            technical_report: '2000',
            technical_report_year: '2001',
            disposition: '456',
            date: '2022-01-04',
            office_department: '22',
            municipality: 'Municipio',
            section: '22',
            chacra: '1234',
            apple: '1234',
            parcela: '3456',
            initiator_name: 'John Breakfast',
            initiator_dni: '234562241',
            kind_of_person: 'Física',
            applicant_name: 'Ricardo Dinner',
            applicant_dni: '53455445',
            applicant_caracter: 'Rare',
            applicant_destination: 'Rarto',
            pdf: 'fileName',
          },
          files: [{ file: { name: 'fileName' } }],
        }
      },
    })

    const spySave = jest.spyOn(wrapper.vm, 'save')

    const cancelBtn = wrapper.find('#saveBtn')
    cancelBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(spySave).toHaveBeenCalledTimes(1)

    await flushPromises()

    expect(wrapper.emitted().save).toBeUndefined()
    expect(wrapper.emitted().messages).toBeTruthy()
    expect(wrapper.emitted().messages[0][0].error).toBeTruthy()
    expect(wrapper.emitted().messages[0][0].message).toBe(
      'Los datos no pudieron ser guardados. Por favor intenta nuevamente.'
    )
  })
})
