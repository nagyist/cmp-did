import Vuex from 'vuex'
import { mount, createLocalVue } from '@vue/test-utils'

import Vuetify from 'vuetify'
import Dervinsa from '~/components/Certificates/Dervinsa6a138.vue'
import { state, mutations, getters } from '~/store/certsConfig.js'

const localVue = createLocalVue()
localVue.use(Vuex)

const $i18n = {
  locale: 'es',
}

const $fire = {
  firestore: {
    collection: jest.fn(() => {
      return {
        add: jest.fn(),
      }
    }),
  },
}

const $fireModule = {
  storage: jest.fn(() => {
    return {
      ref: jest.fn(() => {
        return {
          child: jest.fn(() => {
            return {
              put: jest.fn(),
            }
          }),
        }
      }),
    }
  }),
}

// eslint-disable-next-line import/no-named-as-default-member
const store = new Vuex.Store({
  modules: {
    certsConfig: {
      namespaced: true,
      state,
      mutations,
      getters,
    },
  },
})

const flushPromises = () => new Promise((resolve) => setTimeout(resolve, 0))

describe('Check MisionesBVCerts functionalities', () => {
  let vuetify

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('Clicking the Cancel button emits to close the dialog', async () => {
    const wrapper = mount(Dervinsa, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyCancel = jest.spyOn(wrapper.vm, 'closeDialog')

    const cancelBtn = wrapper.find('#cancelBtn')
    cancelBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(spyCancel).toHaveBeenCalledTimes(1)
    expect(wrapper.emitted().closeDialog).toBeTruthy()
  })

  it('The updateCalcium method assigns the name correctly from the file uploaded from the FilePond component', async () => {
    const wrapper = mount(Dervinsa, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyUpdateCalcium = jest.spyOn(wrapper.vm, 'updateCalcium')

    const filesMock = [{ file: { name: 'fileNameCalcium' } }]

    await wrapper.vm.updateCalcium(filesMock)

    expect(spyUpdateCalcium).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.filesCalcium).toStrictEqual(filesMock)
    expect(wrapper.vm.certificado.calcium).toBe('fileNameCalcium')
  })

  it('The updateCalcium method assigns empty filename if no name is found', async () => {
    const wrapper = mount(Dervinsa, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyUpdateCalcium = jest.spyOn(wrapper.vm, 'updateCalcium')

    const filesMock = []

    await wrapper.vm.updateCalcium(filesMock)

    expect(spyUpdateCalcium).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.filesCalcium).toStrictEqual(filesMock)
    expect(wrapper.vm.certificado.calcium).toBe('')
  })

  it('The updatePurchasing method assigns the name correctly from the file uploaded from the FilePond component', async () => {
    const wrapper = mount(Dervinsa, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyUpdatePurchasing = jest.spyOn(wrapper.vm, 'updatePurchasing')

    const filesMock = [{ file: { name: 'fileNamePurchasing' } }]

    await wrapper.vm.updatePurchasing(filesMock)

    expect(spyUpdatePurchasing).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.filesPurchasing).toStrictEqual(filesMock)
    expect(wrapper.vm.certificado.purchasing).toBe('fileNamePurchasing')
  })

  it('The updatePurchasing method assigns empty filename if no name is found', async () => {
    const wrapper = mount(Dervinsa, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyUpdatePurchasing = jest.spyOn(wrapper.vm, 'updatePurchasing')

    const filesMock = []

    await wrapper.vm.updatePurchasing(filesMock)

    expect(spyUpdatePurchasing).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.filesPurchasing).toStrictEqual(filesMock)
    expect(wrapper.vm.certificado.purchasing).toBe('')
  })

  it('The updateTartaric method assigns the name correctly from the file uploaded from the FilePond component', async () => {
    const wrapper = mount(Dervinsa, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyUpdateTartaric = jest.spyOn(wrapper.vm, 'updateTartaric')

    const filesMock = [{ file: { name: 'fileNameTartaric' } }]

    await wrapper.vm.updateTartaric(filesMock)

    expect(spyUpdateTartaric).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.filesTartaric).toStrictEqual(filesMock)
    expect(wrapper.vm.certificado.acid).toBe('fileNameTartaric')
  })

  it('The updateTartaric method assigns empty filename if no name is found', async () => {
    const wrapper = mount(Dervinsa, {
      localVue,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $i18n,
      },

      stubs: { 'client-only': true, FilePond: true },
    })

    const spyUpdateTartaric = jest.spyOn(wrapper.vm, 'updateTartaric')

    const filesMock = []

    await wrapper.vm.updateTartaric(filesMock)

    expect(spyUpdateTartaric).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.filesTartaric).toStrictEqual(filesMock)
    expect(wrapper.vm.certificado.acid).toBe('')
  })

  it('Check that the save method works correctly and emits the correct message', async () => {
    const wrapper = mount(Dervinsa, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
        $fireModule,
      },

      stubs: { 'client-only': true, FilePond: true },

      data() {
        return {
          certificado: {
            acid: 'fileNameTartaric',
            purchasing: 'fileNamePurchasing',
            calcium: 'fileNameCalcium',
          },
          filesCalcium: [{ file: { name: 'fileNameCalcium' } }],
          filesPurchasing: [{ file: { name: 'fileNamePurchasing' } }],
          filesTartaric: [{ file: { name: 'fileNameTartaric' } }],
        }
      },
    })

    const spySave = jest.spyOn(wrapper.vm, 'save')

    const cancelBtn = wrapper.find('#saveBtn')
    cancelBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(spySave).toHaveBeenCalledTimes(1)

    await flushPromises()

    expect(wrapper.emitted().save).toBeTruthy()
    expect(wrapper.emitted().messages).toBeTruthy()
    expect(wrapper.emitted().messages[0][0].error).toBeFalsy()
    expect(wrapper.emitted().messages[0][0].message).toBe(
      'Los datos fueron guardados.'
    )
  })

  it('Catch test for save method works correctly and emits the correct message', async () => {
    const wrapper = mount(Dervinsa, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fireModule,
      },

      stubs: { 'client-only': true, FilePond: true },

      data() {
        return {
          certificado: {
            acid: 'fileNameTartaric',
            purchasing: 'fileNamePurchasing',
            calcium: 'fileNameCalcium',
          },
          filesCalcium: [{ file: { name: 'fileNameCalcium' } }],
          filesPurchasing: [{ file: { name: 'fileNamePurchasing' } }],
          filesTartaric: [{ file: { name: 'fileNameTartaric' } }],
        }
      },
    })

    const spySave = jest.spyOn(wrapper.vm, 'save')

    const cancelBtn = wrapper.find('#saveBtn')
    cancelBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(spySave).toHaveBeenCalledTimes(1)

    await flushPromises()

    expect(wrapper.emitted().save).toBeUndefined()
    expect(wrapper.emitted().messages).toBeTruthy()
    expect(wrapper.emitted().messages[0][0].error).toBeTruthy()
    expect(wrapper.emitted().messages[0][0].message).toBe(
      'Los datos no pudieron ser guardados. Por favor intenta nuevamente.'
    )
  })

  it('Button for saving should be disabled if no files are uploaded', () => {
    const wrapper = mount(Dervinsa, {
      localVue,
      vuetify,
      store,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fireModule,
      },

      stubs: { 'client-only': true, FilePond: true },

      data() {
        return {
          certificado: {
            acid: '',
            purchasing: '',
            calcium: '',
          },
          filesCalcium: [],
          filesPurchasing: [],
          filesTartaric: [],
        }
      },
    })

    const cancelBtn = wrapper.find('#saveBtn')
    expect(cancelBtn.props().disabled).toBeTruthy()
  })
})
