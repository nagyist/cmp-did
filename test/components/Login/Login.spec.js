import Vuex from 'vuex'
import { mount, createLocalVue } from '@vue/test-utils'

import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import Login from '~/components/Login/Login.vue'
import { state, mutations, getters } from '~/store/authconfig.js'

const localVue = createLocalVue()
localVue.use(Vuex)

localVue.use(VueRouter)
const router = new VueRouter()

const $i18n = {
  locale: 'es',
}

const $fire = {
  firestore: {
    collection: jest.fn(() => {
      return {
        doc: jest.fn(() => {
          return {
            get: jest.fn(() => {
              return {
                data: jest.fn(),
              }
            }),
          }
        }),
      }
    }),
  },
  auth: {
    signInWithEmailAndPassword: jest
      .fn()
      .mockReturnValue({ user: { uid: '283o4nucop289347' } }),
  },
}

// eslint-disable-next-line import/no-named-as-default-member
const store = new Vuex.Store({
  modules: {
    authconfig: {
      namespaced: true,
      state,
      mutations,
      getters,
    },
  },
})

const flushPromises = () => new Promise((resolve) => setTimeout(resolve, 600))

describe('Check components/Login functionalities', () => {
  let vuetify
  let wrapper

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('Read inputs and variables to exists and be empty on mounting', () => {
    wrapper = mount(Login, {
      localVue,
      vuetify,
      store,
      router,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
      },
    })
    expect(wrapper.vm.email).toBe('')
    expect(wrapper.vm.password).toBe('')
  })

  it('Clicking the Send button trigger the login method correctly and pushes route to /', async () => {
    router.push({ path: '/login' })
    wrapper = mount(Login, {
      localVue,
      vuetify,
      store,
      router,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
      },

      data() {
        return {
          password: '*******',
          email: 'joker@hotmail.com',
        }
      },

      //   stubs: { 'client-only': true, FilePond: true },
    })

    const spyLogin = jest.spyOn(wrapper.vm, 'login')

    const loginBtn = wrapper.find('#saveUserForm')
    loginBtn.trigger('submit.prevent')
    await wrapper.vm.$nextTick()

    // console.log(loginBtn.props().disabled)
    // console.log(wrapper.vm.wallet)
    // console.log(wrapper.vm.walletType)
    // console.log(wrapper.vm.loading)

    // wrapper.vm.login()

    await flushPromises()

    expect(spyLogin).toHaveBeenCalledTimes(1)
    expect(wrapper.emitted().wait).toBeTruthy()
    expect(router.currentRoute.path).toBe('/')
  })

  it('Clicking the Send button trigger the login method correctly and pushes route to /complete-data when theres no public address', async () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const $store = new Vuex.Store({
      state: {},
      getters: {
        'authconfig/autenticar': () => {
          return {
            autenticar: true,
            email: false,
            mobile: false,
            wallet: false,
            register: true,
            did: true,
          }
        },
      },
    })

    const $fire = {
      firestore: {
        collection: jest.fn(() => {
          return {
            doc: jest.fn(() => {
              return {
                get: jest.fn(() => {
                  return {
                    data: jest.fn().mockReturnValue({ public_address: null }),
                  }
                }),
              }
            }),
          }
        }),
      },
      auth: {
        signInWithEmailAndPassword: jest
          .fn()
          .mockReturnValue({ user: { uid: '283o4nucop289347' } }),
      },
    }

    wrapper = mount(Login, {
      localVue,
      vuetify,
      router,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
        $store,
      },

      data() {
        return {
          password: '*******',
          email: 'joker@hotmail.com',
        }
      },
    })

    const spyLogin = jest.spyOn(wrapper.vm, 'login')

    const loginBtn = wrapper.find('#saveUserForm')
    loginBtn.trigger('submit.prevent')
    await wrapper.vm.$nextTick()

    await flushPromises()

    expect(spyLogin).toHaveBeenCalledTimes(1)
    expect(wrapper.emitted().wait).toBeTruthy()
    expect(router.currentRoute.fullPath).toBe('/complete-data?step=3')
  })

  it('Clicking the Send button trigger the login method correctly and pushes route to /citizen/certificates/ when theres a public address', async () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const $store = new Vuex.Store({
      state: {},
      getters: {
        'authconfig/autenticar': () => {
          return {
            autenticar: true,
            email: false,
            mobile: false,
            wallet: false,
            register: true,
            did: true,
          }
        },
      },
    })

    const $fire = {
      firestore: {
        collection: jest.fn(() => {
          return {
            doc: jest.fn(() => {
              return {
                get: jest.fn(() => {
                  return {
                    data: jest
                      .fn()
                      .mockReturnValue({ public_address: '1234abcd' }),
                  }
                }),
              }
            }),
          }
        }),
      },
      auth: {
        signInWithEmailAndPassword: jest
          .fn()
          .mockReturnValue({ user: { uid: '283o4nucop289347' } }),
      },
    }

    wrapper = mount(Login, {
      localVue,
      vuetify,
      router,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
        $store,
      },

      data() {
        return {
          password: '*******',
          email: 'joker@hotmail.com',
        }
      },
    })

    const spyLogin = jest.spyOn(wrapper.vm, 'login')

    const loginBtn = wrapper.find('#saveUserForm')
    loginBtn.trigger('submit.prevent')
    await wrapper.vm.$nextTick()

    await flushPromises()

    expect(spyLogin).toHaveBeenCalledTimes(1)
    expect(wrapper.emitted().wait).toBeTruthy()
    expect(router.currentRoute.fullPath).toBe('/citizen/certificates/')
  })

  it('Clicking the Send button without the correct info, goes to the catch clause', async () => {
    // eslint-disable-next-line import/no-named-as-default-member

    const $fire = {
      firestore: {
        collection: jest.fn(() => {
          return {
            doc: jest.fn(() => {
              return {
                get: jest.fn(() => {
                  return {
                    data: jest
                      .fn()
                      .mockReturnValue({ public_address: '1234abcd' }),
                  }
                }),
              }
            }),
          }
        }),
      },
      auth: {
        signInWithEmailAndPassword: jest
          .fn()
          .mockReturnValue({ user: { uid: '283o4nucop289347' } }),
      },
    }

    wrapper = mount(Login, {
      localVue,
      vuetify,
      router,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
      },

      data() {
        return {
          password: '*******',
          email: 'joker@hotmail.com',
        }
      },
    })

    const spyLogin = jest.spyOn(wrapper.vm, 'login')

    const loginBtn = wrapper.find('#saveUserForm')
    loginBtn.trigger('submit.prevent')
    await wrapper.vm.$nextTick()

    await flushPromises()

    expect(spyLogin).toHaveBeenCalledTimes(1)
    expect(wrapper.emitted().wait).toBeTruthy()
    expect(wrapper.emitted().wait[1]).toStrictEqual([false])
  })

  it('public_address computed returns the wallet of the user', async () => {
    wrapper = mount(Login, {
      localVue,
      vuetify,
      store,
      router,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
      },
    })
    await wrapper.setProps({ wallet: '0a9s78dt7381jenm' })
    expect(wrapper.vm.public_address).toBe('0a9s78dt7381jenm')
  })

  it('web3 computed returns the provider of the service', async () => {
    wrapper = mount(Login, {
      localVue,
      vuetify,
      store,
      router,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
      },
    })
    await wrapper.setProps({ provider: 'provider' })
    expect(wrapper.vm.web3).toBe('provider')
  })

  it('closeDialog emits an event to parent component correctly', () => {
    wrapper = mount(Login, {
      localVue,
      vuetify,
      store,
      router,

      mocks: {
        $t: (msg) => msg,
        $i18n,
        $fire,
      },
    })

    wrapper.vm.closeDialog()
    expect(wrapper.emitted().handleDialogLogin).toBeTruthy()
    expect(wrapper.emitted().handleDialogLogin[0]).toStrictEqual([false])
  })
})
