import Vuex from 'vuex'
import Vuetify from 'vuetify'
import { mount, createLocalVue } from '@vue/test-utils'

import { mutations } from '~/store/index'
import Header from '~/components/public/Header.vue'

document.body.setAttribute('data-app', true)

const $fire = {
  storage: {
    ref: jest.fn(() => {
      return {
        child: jest.fn(() => {
          return {
            put: jest.fn().mockImplementation(() => Promise.resolve(true)),
          }
        }),
      }
    }),
  },
  firestore: {
    collection: jest.fn(() => {
      return {
        doc: jest.fn(() => {
          return {
            set: jest.fn(),
          }
        }),
      }
    }),
  },
}

const flushPromises = () => new Promise((resolve) => setTimeout(resolve, 0))

describe('Logout screen', () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  let vuetify
  let wrapper
  const $t = () => {}
  // eslint-disable-next-line import/no-named-as-default-member
  const store = new Vuex.Store({
    mutations,
    state: {},
    getters: {
      'users/user': () => {
        return {
          first_name: '',
          last_name: '',
          email: '',
          password: '',
          dni: '',
          public_address: '',
        }
      },
      'brand/brand': () => {
        return {
          header_background_type: '',
        }
      },
    },
  })

  beforeEach(() => {
    vuetify = new Vuetify()
    wrapper = mount(Header, {
      localVue,
      store,
      vuetify,
      mocks: {
        $t,
      },
      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })
  })

  test('Is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy()
  })

  test('When logged out, login button should be present', () => {
    if (store.getters['authconfig/email']) {
      expect(wrapper.find('#login').isVisible()).toBe(true)
    }
  })

  test('When logged in, login button should not exist', () => {
    store.commit('ON_AUTH_STATE_CHANGED_MUTATION', {
      authUser: {
        uid: '',
        email: 'pamela@os.city',
        emailVerified: true,
        phoneNumber: '',
      },
    })
    wrapper = mount(Header, {
      localVue,
      store,
      vuetify,
      mocks: {
        $t,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },
      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })
    expect(wrapper.find('#login').exists()).toBe(false)
  })
})

describe('Test Header Methods Functionalities', () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  let vuetify

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('If user is superadmin, v-dialog for changing colors should be present', async () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      mutations,
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Pepe',
            last_name: 'Aguilar',
            email: 'joker@email.com',
            password: '****',
            dni: '234546',
            public_address: 'sdf8sa6df7yuhbn',
            type: 'superadmin',
          }
        },
        'brand/brand': () => {
          return {
            header_background_type: '',
          }
        },
      },
    })

    const wrapper = mount(Header, {
      localVue,
      store,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },

      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })

    const headerBtn = wrapper.find('#btnDialogHeader')
    headerBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(headerBtn.exists()).toBeTruthy()
    expect(wrapper.find('.v-toolbar').exists()).toBe(true)
  })

  it('The autenticar method works correctly', () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      mutations,
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Pepe',
            last_name: 'Aguilar',
            email: 'joker@email.com',
            password: '****',
            dni: '234546',
            public_address: 'sdf8sa6df7yuhbn',
            type: 'superadmin',
          }
        },
        'brand/brand': () => {
          return {
            header_background_type: '',
          }
        },
      },
    })

    const $autenticar = {
      init: jest.fn(),
    }

    const wrapper = mount(Header, {
      localVue,
      store,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $autenticar,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },

      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })

    const spyAutenticar = jest.spyOn(wrapper.vm, 'autenticar')

    wrapper.vm.autenticar()
    expect(spyAutenticar).toHaveBeenCalledTimes(1)
    expect($autenticar.init).toHaveBeenCalledTimes(1)
  })

  it('Clicking the save button triggers the updateBrand method correctly', async () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      mutations: {
        'brand/SET_BRAND': () => {},
      },
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Pepe',
            last_name: 'Aguilar',
            email: 'joker@email.com',
            password: '****',
            dni: '234546',
            public_address: 'sdf8sa6df7yuhbn',
            type: 'superadmin',
          }
        },
        'brand/brand': () => {
          return {
            header_background_type: '',
          }
        },
      },
    })

    const wrapper = mount(Header, {
      localVue,
      store,
      vuetify,

      mocks: {
        $fire,
        $t: (msg) => msg,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },

      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })

    const headerBtn = wrapper.find('#btnDialogHeader')
    headerBtn.trigger('click')
    await wrapper.vm.$nextTick()

    const spySave = jest.spyOn(wrapper.vm, 'updateBrand')

    const saveBtn = wrapper.find('#saveBtn')
    saveBtn.trigger('click')
    await wrapper.vm.$nextTick()

    await flushPromises()

    expect(spySave).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.loading).toBe(false)
    expect(wrapper.vm.isEditing).toBe(false)
    expect(wrapper.vm.header_dialog).toBe(false)
  })

  it('Save button catch method assigns snackbar variables the correct values', async () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      mutations: {
        'brand/SET_BRAND': () => {},
      },
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Pepe',
            last_name: 'Aguilar',
            email: 'joker@email.com',
            password: '****',
            dni: '234546',
            public_address: 'sdf8sa6df7yuhbn',
            type: 'superadmin',
          }
        },
        'brand/brand': () => {
          return {
            header_background_type: '',
          }
        },
      },
    })

    const wrapper = mount(Header, {
      localVue,
      store,
      vuetify,

      mocks: {
        $t: (msg) => msg,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },

      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })

    const headerBtn = wrapper.find('#btnDialogHeader')
    headerBtn.trigger('click')
    await wrapper.vm.$nextTick()

    const spySave = jest.spyOn(wrapper.vm, 'updateBrand')

    const saveBtn = wrapper.find('#saveBtn')
    saveBtn.trigger('click')
    await wrapper.vm.$nextTick()

    await flushPromises()

    expect(spySave).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.snackbar.show).toBe(true)
    expect(wrapper.vm.snackbar.text).toBe('No se pudo guardar la actualización')
    expect(wrapper.vm.snackbar.color).toBe('error')
  })

  it('Clicking the restore button triggers the restoreDefaultLogo method correctly', async () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      mutations: {
        'brand/SET_BRAND': () => {},
      },
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Pepe',
            last_name: 'Aguilar',
            email: 'joker@email.com',
            password: '****',
            dni: '234546',
            public_address: 'sdf8sa6df7yuhbn',
            type: 'superadmin',
          }
        },
        'brand/brand': () => {
          return {
            header_background_type: '',
          }
        },
      },
    })

    const wrapper = mount(Header, {
      localVue,
      store,
      vuetify,

      mocks: {
        $fire,
        $t: (msg) => msg,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },

      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })

    const headerBtn = wrapper.find('#btnDialogHeader')
    headerBtn.trigger('click')
    await wrapper.vm.$nextTick()

    const spyRestore = jest.spyOn(wrapper.vm, 'restoreDefaultLogo')

    const saveBtn = wrapper.find('#restoreBtn')
    saveBtn.trigger('click')
    await wrapper.vm.$nextTick()

    expect(spyRestore).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.brandSettings.logo).toBe(
      'https://firebasestorage.googleapis.com/v0/b/playground-gcp-201118.appspot.com/o/brand%2F4010fc67-09a4-41a8-8851-29fc2a45d6bb?alt=media&token=984cee1d-2955-4231-807d-21dfa45b8746'
    )
  })

  it('Changing the link in the image editor, triggers the onFileSelected method correctly', async () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      mutations: {
        'brand/SET_BRAND': () => {},
      },
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Pepe',
            last_name: 'Aguilar',
            email: 'joker@email.com',
            password: '****',
            dni: '234546',
            public_address: 'sdf8sa6df7yuhbn',
            type: 'superadmin',
          }
        },
        'brand/brand': () => {
          return {
            header_background_type: '',
          }
        },
      },
    })

    const wrapper = mount(Header, {
      localVue,
      store,
      vuetify,

      mocks: {
        $fire,
        $t: (msg) => msg,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },

      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })

    wrapper.vm.onFileSelected({ target: { id: '123', files: ['lorem.pdf'] } })

    await flushPromises()

    expect(wrapper.vm.loading).toBe(false)
    expect(wrapper.vm.isEditing).toBe(false)
  })

  it('onFileSelected catch should change the snackbar variables', async () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      mutations: {
        'brand/SET_BRAND': () => {},
      },
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Pepe',
            last_name: 'Aguilar',
            email: 'joker@email.com',
            password: '****',
            dni: '234546',
            public_address: 'sdf8sa6df7yuhbn',
            type: 'superadmin',
          }
        },
        'brand/brand': () => {
          return {
            header_background_type: '',
          }
        },
      },
    })

    const $fire = {
      storage: {
        ref: jest.fn(() => {
          return {
            child: jest.fn(() => {
              return {
                put: jest
                  .fn()
                  .mockImplementation(() =>
                    Promise.reject(new Error('Something went wrong'))
                  ),
              }
            }),
          }
        }),
      },
      firestore: {
        collection: jest.fn(() => {
          return {
            doc: jest.fn(() => {
              return {
                set: jest.fn(),
              }
            }),
          }
        }),
      },
    }

    const wrapper = mount(Header, {
      localVue,
      store,
      vuetify,

      mocks: {
        $fire,
        $t: (msg) => msg,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },

      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })

    wrapper.vm.onFileSelected({ target: { id: '123', files: ['lorem.pdf'] } })

    await flushPromises()

    expect(wrapper.vm.snackbar.show).toBe(true)
    expect(wrapper.vm.snackbar.text).toBe('Something went wrong')
    expect(wrapper.vm.snackbar.color).toBe('error')
  })

  it('In case the background is lineal, the computed property header_background should return the primary color in the store', () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      mutations: {
        'brand/SET_BRAND': () => {},
      },
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Pepe',
            last_name: 'Aguilar',
            email: 'joker@email.com',
            password: '****',
            dni: '234546',
            public_address: 'sdf8sa6df7yuhbn',
            type: 'superadmin',
          }
        },
        'brand/brand': () => {
          return {
            header_background_type: 'lineal',
            header_bg_primary: 'rojo',
          }
        },
      },
    })

    const wrapper = mount(Header, {
      localVue,
      store,
      vuetify,

      mocks: {
        $fire,
        $t: (msg) => msg,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },

      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })

    expect(wrapper.vm.header_background).toBe('background-color: rojo')
  })

  it('In case the background is gradient, the computed property header_background should return both colors in the store', () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      mutations: {
        'brand/SET_BRAND': () => {},
      },
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Pepe',
            last_name: 'Aguilar',
            email: 'joker@email.com',
            password: '****',
            dni: '234546',
            public_address: 'sdf8sa6df7yuhbn',
            type: 'superadmin',
          }
        },
        'brand/brand': () => {
          return {
            header_background_type: 'gradient',
            header_bg_primary: 'rojo',
            header_bg_secondary: 'amarillo',
          }
        },
      },
    })

    const wrapper = mount(Header, {
      localVue,
      store,
      vuetify,

      mocks: {
        $fire,
        $t: (msg) => msg,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },

      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })

    expect(wrapper.vm.header_background).toBe(
      'background-image: linear-gradient(to right, rojo, amarillo'
    )
  })

  it('In case no background is specified, the computed property header_background should return the default colors', () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      mutations: {
        'brand/SET_BRAND': () => {},
      },
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Pepe',
            last_name: 'Aguilar',
            email: 'joker@email.com',
            password: '****',
            dni: '234546',
            public_address: 'sdf8sa6df7yuhbn',
            type: 'superadmin',
          }
        },
        'brand/brand': () => {
          return {
            header_background_type: 'none',
            header_bg_primary: '',
            header_bg_secondary: '',
          }
        },
      },
    })

    const wrapper = mount(Header, {
      localVue,
      store,
      vuetify,

      mocks: {
        $fire,
        $t: (msg) => msg,
        $vuetify: {
          breakpoint: {
            mdAndUp: true,
          },
        },
      },

      stubs: {
        NuxtLink: true,
        Logo: true,
        Login: true,
        'Mobile-Auth': true,
        LoginWallet: true,
        EmailRegister: true,
        Sidebar: true,
        'router-link': true,
      },
    })

    expect(wrapper.vm.header_background).toBe(
      'background-image: linear-gradient(to right, #0096da, #035d98)'
    )
  })
})
