import Vuetify from 'vuetify'
import Vuex, { Store } from 'vuex'
import VueRouter from 'vue-router'
import { mount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import axios from 'axios'
import Sidebar from '@/components/public/Sidebar.vue'

const emptyBrand = {
  app_name: '',
  city_splash_primary:
    'https://firebasestorage.googleapis.com/v0/b/demosburg-dev.appspot.com/o/brand%2Fimages%2Fdefault_splash_primary.png?alt=media&token=b4feafa8-08ba-4396-9fc0-1fbbed2cf5e4',
  city_splash_secondary:
    'https://firebasestorage.googleapis.com/v0/b/demosburg-dev.appspot.com/o/brand%2Fimages%2Fdefault_splash_secondary.png?alt=media&token=616244e7-e306-498a-a3b3-3d138c15f10f',
  city_splash_login: '',
  city_url: 'demo.os.city',
  city_name: '',
  copyright_footer: '',
  favicon: '',
  logo: null,
  logo_provincia_misiones: null,
  primary_color: '#e5528e',
  secondary_color: '#ad57b2',
  header_color: '#ffffff',
  footer_color: '#ffffff',
  footer_contact_button_color: '',
  footer_background_type: 'lineal',
  footer_bg_primary: '',
  footer_bg_secondary: '',
  footer_logo: '',
  footer_text_color: '',
  textHeaderColor: '#ffffff',
  textFooterColor: '#ffffff',
  heightLogo: '40px',
  additional_color: '#d6d6d6',
  about_title: 'Acerca de',
  about_content: '',
  contact_title: 'Contacto',
  contact_info: '',
  hero_bg_primary: '#5d306f',
  hero_bg_secondary: '#bd0b4d',
  terms: false,
  privacy: false,
  termsAndConditions: '',
  privayPolicy: '',
  needsInfo: true,
  buttonColorLogin: '#43475C',
  mobileStepThree: '',
  firstImageStepThree: '',
  secondImageStepThree: '',
  thirdImageStepThree: '',
  header_bg_primary: '#0096da',
  header_bg_secondary: '#035d98',
  header_button_color: '#0088CC',
  header_background_type: 'lineal',
  errorImage: '',
  hero_background_type: 'lineal',
  hero_bg_image: '',
  levelUpImage: '',
  validateLevelImage: '',
  appStoreDowload: '',
  qrImage: '',
  loaded: false,
  avatar_text_color: '#FFFFFF',
}

const mocks = {
  $t: (msg) => msg,
  $fireModule: {
    auth: {
      RecaptchaVerifier: jest.fn().mockImplementation(() => {
        return {
          render: jest.fn(() => Promise.resolve(1111)),
        }
      }),
    },
  },
  $fire: {
    firestore: {
      collection: jest.fn(() => {
        return {
          limit: jest.fn(() => {
            return {
              get: jest.fn(() =>
                Promise.resolve({
                  docs: [
                    {
                      data: () => {
                        return {
                          playstore_link:
                            'https://play.google.com/store/apps/details?id=com.oscity.oswallet',
                          playstore_icon:
                            'https://play.google.com/intl/es-419/badges/static/images/badges/es-419_badge_web_generic.png',
                        }
                      },
                    },
                  ],
                })
              ),
            }
          }),
          doc: jest.fn(() => {
            return {
              set: jest.fn(),
            }
          }),
        }
      }),
    },
  },
}

const mutations = {
  'brand/SET_BRAND': (state, brand) => {
    state.brand = brand
  },
  'users/SET_USER': (state, user) => {
    state.user.type = user.type ? user.type : null
    state.user = { ...state.user, ...user }
  },
}

const emptyUser = {
  first_name: '',
  last_name: '',
  email: '',
  password: '',
  dni: '',
  public_address: '',
}
const emptyLandingText = {
  heroSection: {
    title: '',
    subtitle: '',
    second_subtitle: '',
  },
  headerSection: {
    login_button: 'Iniciar sesión',
    register_button: 'Registro',
  },
  columnsSection: {
    city: '',
    slogan: '',
    title_column1: '',
    title_column2: '',
    title_column3: '',
    info_column1: '',
    info_column2: '',
    info_column3: '',
  },
  reminderSection: {
    reminderTitle: '',
    reminderText: '',
  },
  certificateSection: {
    certificate_title: '',
  },
  digitalCitizen: {
    title: '',
    subtitle: '',
    levels_description: '',
    step_1: {
      description: '',
    },
    step_2: {
      description: '',
    },
    step_3: {
      description: '',
    },
    step_4: {
      description: '',
    },
    step_5: {
      description: '',
    },
  },
  whatIsCidi: {
    section_1: {
      title: '',
      description: '',
    },
    section_2: {
      title: '',
      description: '',
    },
    section_3: {
      title: '',
      description: '',
    },
    section_4: {
      title: '',
      description: '',
    },
  },
  step_number_color: '',
  loaded: false,
}

const createStore = (params) => {
  return new Store({
    state: {
      user: { ...emptyUser, ...params?.user },
      brand: {
        ...emptyBrand,
        ...params?.brand,
      },
      landingtext: {
        ...emptyLandingText,
        ...params?.landingText,
      },
      authconfig: {
        isDID: params?.isDID || false,
      },
      loggedIn: params?.loggedIn || false,
    },

    getters: {
      'users/user': (state) => state.user,
      'brand/brand': (state) => state.brand,
      'landingtext/landingtext': (state) => state.landingtext,
      'authconfig/isDID': (state) => state.authconfig.isDID,
      loggedIn: (state) => state.loggedIn,
    },
    mutations,
    // actions: {
    //   'users/fetchUser': () => {
    //     return jest.fn()
    //   },
    //   setloggedIn: () => {
    //     return jest.fn()
    //   },
    // },
  })
}

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
document.body.setAttribute('data-app', true)

jest.mock('axios')

describe('Testing Sidebar', () => {
  let vuetify
  let wrapper
  let store

  const router = new VueRouter()

  beforeEach(() => {
    vuetify = new Vuetify()
  })
  afterEach(() => {
    wrapper.destroy()
    window.localStorage.clear()
  })

  test('should mount navigation drawer component succesfully', async () => {
    store = createStore()

    wrapper = await mount(Sidebar, {
      props: { value: true },
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.find('.v-navigation-drawer').exists()).toBe(true)
  })

  test('If user have uid and is registered with email and has public address userVerified should be true', async () => {
    store = createStore()

    wrapper = await mount(Sidebar, {
      props: { value: true },
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.vm.userVerified).toBe(true)
  })

  test('When user is not logged in, login button should be mounted', async () => {
    store = createStore()

    wrapper = await mount(Sidebar, {
      props: { value: true },
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.find('#login-button').exists()).toBe(true)
  })

  test('When user is logged in, login button should not be mounted', async () => {
    store = createStore({ loggedIn: true })

    wrapper = await mount(Sidebar, {
      props: { value: true },
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.find('#login-button').exists()).toBe(false)
  })

  test('Hero_background should be dinamic according to its style', async () => {
    store = createStore({
      brand: {
        hero_background_type: 'lineal',
        hero_bg_primary: 'black',
      },
    })

    wrapper = await mount(Sidebar, {
      props: { value: true },
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.vm.hero_background).toBe('background-color: black')
    await store.commit('brand/SET_BRAND', {
      hero_background_type: 'gradient',
      hero_bg_primary: 'black',
      hero_bg_secondary: 'white',
    })
    expect(wrapper.vm.hero_background).toBe(
      'background-image: linear-gradient(to bottom, black, white)'
    )
    await store.commit('brand/SET_BRAND', {
      hero_background_type: 'image',
      hero_bg_primary: 'black',
      hero_bg_secondary: 'white',
      hero_bg_image: 'https://example.com/images/default.png',
    })
    expect(wrapper.vm.hero_background).toBe(
      'background-image: url(https://example.com/images/default.png); background-size: cover; background-position: center;'
    )
    await store.commit('brand/SET_BRAND', {
      hero_background_type: 'error',
      hero_bg_primary: 'black',
      hero_bg_secondary: 'white',
      hero_bg_image: 'https://example.com/images/default.png',
    })
    expect(wrapper.vm.hero_background).toBe(
      'background-image: linear-gradient(to bottom, #5d306f, #bd0b4d)'
    )
  })
  // TODO: TEST ANOTHER BACKGROUND TYPES.

  test('If user changes it should verify his registration', async () => {
    store = createStore({
      user: {
        uid: '123',
      },
    })

    wrapper = await mount(Sidebar, {
      props: { value: true },
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    const spy = jest.spyOn(wrapper.vm, 'verifyUserRegistration')

    await store.commit('users/SET_USER', { uid: '456' })
    expect(spy).toBeCalledTimes(1)
  })

  test("If it's a Dervinsa instance, the title of the first operator menu should be 'Acid'", async () => {
    store = createStore({})

    wrapper = await mount(Sidebar, {
      props: { value: true },
      localVue,
      vuetify,
      store,
      mocks,
      router,
      data: () => {
        return { isDervinsa: true }
      },
      stubs: { NuxtLink: RouterLinkStub },
    })

    expect(wrapper.vm.certificatesMenuOperator[0][0]).toBe('Acid')
  })

  test("If user has email registered and has providerData, userIsRegistered should be true'", async () => {
    store = createStore({
      user: {
        uid: '123',
      },
    })

    axios.post.mockImplementation(() =>
      Promise.resolve({
        status: 200,
        data: {
          user: {
            email: 'test@example.com',
            providerData: ['test', 'test1'],
          },
        },
      })
    )

    wrapper = await mount(Sidebar, {
      props: { value: true },
      localVue,
      vuetify,
      store,
      mocks: { ...mocks, axios },
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.userIsRegistered).toBe(true)
  })

  test("If user in store have a public address, 'userHavePublicAddress' attribute should be true", async () => {
    store = createStore({
      user: {
        uid: '123',
        public_address: 'kfnwurnzhwurnxywejvn123',
      },
    })

    axios.post.mockImplementation(() =>
      Promise.resolve({
        status: 200,
        data: {
          user: {
            email: 'test@example.com',
            providerData: ['test', 'test1'],
          },
        },
      })
    )

    wrapper = await mount(Sidebar, {
      props: { value: true },
      localVue,
      vuetify,
      store,
      mocks: { ...mocks, axios },
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.userHavePublicAddress).toBe(true)
  })
})
