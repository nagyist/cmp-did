import Vuetify from 'vuetify'
import Vuex, { Store } from 'vuex'
import VueRouter from 'vue-router'
import { mount, createLocalVue, RouterLinkStub } from '@vue/test-utils'
import axios from 'axios'
import swal from 'sweetalert'
import Footer from '@/components/public/Footer.vue'
import Contact from '@/components/public/Contact.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)
document.body.setAttribute('data-app', true)

const emptyUser = {
  first_name: '',
  last_name: '',
  email: '',
  password: '',
  dni: '',
  public_address: '',
}

const emptyBrand = {
  app_name: '',
  city_splash_primary:
    'https://firebasestorage.googleapis.com/v0/b/demosburg-dev.appspot.com/o/brand%2Fimages%2Fdefault_splash_primary.png?alt=media&token=b4feafa8-08ba-4396-9fc0-1fbbed2cf5e4',
  city_splash_secondary:
    'https://firebasestorage.googleapis.com/v0/b/demosburg-dev.appspot.com/o/brand%2Fimages%2Fdefault_splash_secondary.png?alt=media&token=616244e7-e306-498a-a3b3-3d138c15f10f',
  city_splash_login: '',
  city_url: 'demo.os.city',
  city_name: '',
  copyright_footer: '',
  favicon: '',
  logo: null,
  logo_provincia_misiones: null,
  primary_color: '#e5528e',
  secondary_color: '#ad57b2',
  header_color: '#ffffff',
  footer_color: '#ffffff',
  footer_contact_button_color: '',
  footer_background_type: 'lineal',
  footer_bg_primary: '',
  footer_bg_secondary: '',
  footer_logo: '',
  footer_text_color: '',
  textHeaderColor: '#ffffff',
  textFooterColor: '#ffffff',
  heightLogo: '40px',
  additional_color: '#d6d6d6',
  about_title: 'Acerca de',
  about_content: '',
  contact_title: 'Contacto',
  contact_info: '',
  hero_bg_primary: '#5d306f',
  hero_bg_secondary: '#bd0b4d',
  terms: false,
  privacy: false,
  termsAndConditions: '',
  privayPolicy: '',
  needsInfo: true,
  buttonColorLogin: '#43475C',
  mobileStepThree: '',
  firstImageStepThree: '',
  secondImageStepThree: '',
  thirdImageStepThree: '',
  header_bg_primary: '#0096da',
  header_bg_secondary: '#035d98',
  header_button_color: '#0088CC',
  header_background_type: 'lineal',
  errorImage: '',
  hero_background_type: 'lineal',
  hero_bg_image: '',
  levelUpImage: '',
  validateLevelImage: '',
  appStoreDowload: '',
  qrImage: '',
  loaded: false,
  avatar_text_color: '#FFFFFF',
}

const mutations = {
  'brand/SET_BRAND': jest.fn(),
}

const createStore = (params) => {
  return new Store({
    state: {
      // loggedIn: isLoggedIn || false,
    },

    getters: {
      'users/user': () => {
        return {
          ...emptyUser,
          ...params?.user,
        }
      },
      'brand/brand': () => {
        return {
          ...emptyBrand,
          ...params?.brand,
        }
      },

      'authconfig/isDID': () => {
        return params?.isDID || false
      },
    },
    mutations,
    // actions: {
    //   'users/fetchUser': () => {
    //     return jest.fn()
    //   },
    //   setloggedIn: () => {
    //     return jest.fn()
    //   },
    // },
  })
}
const mocks = {
  $t: (msg) => msg,
  $fireModule: {
    auth: {
      RecaptchaVerifier: jest.fn(() => ({
        render: jest.fn(() => Promise.resolve(1111)),
      })),
    },
  },
  $fire: {
    firestore: {
      collection: jest.fn(() => {
        return {
          limit: jest.fn(() => {
            return {
              get: jest.fn(() =>
                Promise.resolve({
                  docs: [
                    {
                      data: () => {
                        return {
                          playstore_link:
                            'https://play.google.com/store/apps/details?id=com.oscity.oswallet',
                          playstore_icon:
                            'https://play.google.com/intl/es-419/badges/static/images/badges/es-419_badge_web_generic.png',
                        }
                      },
                    },
                  ],
                })
              ),
            }
          }),
          doc: jest.fn(() => {
            return {
              set: jest.fn(),
            }
          }),
        }
      }),
    },
    storage: {
      ref: jest.fn(() => {
        return {
          child: jest.fn(() => {
            return {
              put: jest.fn(() => {
                return {
                  then: jest.fn(() => Promise.resolve(1111)),
                }
              }),
            }
          }),
        }
      }),
    },
  },
}

jest.mock('axios')
jest.mock('sweetalert')
describe('Testing Footer', () => {
  let vuetify
  let wrapper
  let store

  const router = new VueRouter()

  beforeEach(() => {
    vuetify = new Vuetify()
  })
  afterEach(() => {
    wrapper.destroy()
    window.localStorage.clear()
  })

  test('should mount basic components succesfully', () => {
    store = createStore()

    wrapper = mount(Footer, {
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.find('.v-footer').exists()).toBe(true)
  })

  test("if route change to '/' (home), isHome property should change depending on the route", async () => {
    store = createStore()

    wrapper = mount(Footer, {
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    await router.push('/what-is')
    expect(wrapper.vm.isHome).toBe(false)
    await router.push('/')
    expect(wrapper.vm.isHome).toBe(true)
  })
  test('if cmp is a DID instance should get playstore link and icon from firestore', async () => {
    store = createStore({ isDID: true })

    wrapper = await mount(Footer, {
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.vm.playstore_link).toBe(
      'https://play.google.com/store/apps/details?id=com.oscity.oswallet'
    )
    expect(wrapper.vm.playstore_icon).toBe(
      'https://play.google.com/intl/es-419/badges/static/images/badges/es-419_badge_web_generic.png'
    )
  })
  test('If user is logged in and have SuperAdmin privileges should be able to edit footer style', async () => {
    store = createStore({ user: { type: 'superadmin' } })
    wrapper = await mount(Footer, {
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.find('#editor-button').exists()).toBe(true)
  })

  test("If user is logged in but don't have SuperAdmin privileges, should'nt be able to edit footer style", async () => {
    store = createStore({ user: { type: 'operator' } })
    wrapper = await mount(Footer, {
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.find('#editor-button').exists()).toBe(false)
  })

  test("If user is not logged, should'nt be able to edit footer style", async () => {
    store = createStore({ user: { type: 'operator' } })
    wrapper = await mount(Footer, {
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(wrapper.find('#editor-button').exists()).toBe(false)
  })

  test('Should update brand if save button is clicked on Editor Dialog', async () => {
    store = createStore({ user: { type: 'superadmin' } })

    axios.post.mockImplementation(() =>
      Promise.resolve({
        status: 200,
      })
    )

    const wrapper = await mount(Footer, {
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    await wrapper.find('#editor-button').trigger('click')
    await wrapper.find('#save-button').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.loading).toBe(false)
    expect(wrapper.vm.isEditing).toBe(false)
    expect(wrapper.vm.footer_dialog).toBe(false)
    // console.log(store._mutations['brand/SET_BRAND'])
    expect(mutations['brand/SET_BRAND']).toHaveBeenCalled()
    expect(mocks.$fire.firestore.collection).toHaveBeenCalled()
  })

  test('If there is an error on firebase, should catch the error and show snackbar', async () => {
    store = createStore({ user: { type: 'superadmin' } })

    const $fire = {
      firestore: {
        collection: jest.fn(() => {
          return {
            limit: jest.fn(() => {
              return {
                get: jest.fn(() =>
                  Promise.resolve({
                    docs: [
                      {
                        data: () => {
                          return {
                            playstore_link:
                              'https://play.google.com/store/apps/details?id=com.oscity.oswallet',
                            playstore_icon:
                              'https://play.google.com/intl/es-419/badges/static/images/badges/es-419_badge_web_generic.png',
                          }
                        },
                      },
                    ],
                  })
                ),
              }
            }),
            doc: jest.fn(() => {
              return {
                set: new Error('Error en firestore'),
              }
            }),
          }
        }),
      },
    }

    const wrapper = await mount(Footer, {
      localVue,
      vuetify,
      store,
      mocks: { ...mocks, $fire },
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    await wrapper.find('#editor-button').trigger('click')
    await wrapper.find('#save-button').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.loading).toBe(false)
    expect(wrapper.vm.isEditing).toBe(false)
    expect(wrapper.vm.footer_dialog).toBe(false)
    expect(wrapper.vm.snackbar.show).toBe(true)
    expect(wrapper.vm.snackbar.text).toBe('No se pudo guardar la actualización')
    expect(wrapper.vm.snackbar.color).toBe('error')
  })

  test('Restore logo button should restore default logo on brandSettings', async () => {
    store = createStore({ user: { type: 'superadmin' } })

    const wrapper = await mount(Footer, {
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    await wrapper.find('#editor-button').trigger('click')
    await wrapper.find('#restoreLogo-button').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.brandSettings.footer_logo).toBe(
      'https://firebasestorage.googleapis.com/v0/b/playground-gcp-201118.appspot.com/o/brand%2F4010fc67-09a4-41a8-8851-29fc2a45d6bb?alt=media&token=984cee1d-2955-4231-807d-21dfa45b8746'
    )
  })
  test('Changing the link in the image editor, triggers the onFileSelected method correctly', async () => {
    store = createStore({ user: { type: 'superadmin' } })

    const wrapper = await mount(Footer, {
      localVue,
      vuetify,
      store,
      mocks,
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.vm.onFileSelected({ target: { id: '123', files: ['lorem.pdf'] } })

    await wrapper.vm.$nextTick()

    expect(wrapper.vm.loading).toBe(false)
    expect(wrapper.vm.isEditing).toBe(false)
  })
})

describe('Testing Contact', () => {
  let vuetify
  let wrapper
  let store

  const router = new VueRouter()

  beforeEach(() => {
    vuetify = new Vuetify()
  })
  afterEach(() => {
    wrapper.destroy()
    window.localStorage.clear()
    jest.clearAllMocks()
  })
  test('After submit form, should clean contact data and show a sweetAlert dialog ', async () => {
    store = createStore()

    axios.post.mockImplementation(() =>
      Promise.resolve({
        status: 200,
      })
    )

    wrapper = await mount(Contact, {
      localVue,
      vuetify,
      store,
      mocks: { ...mocks, axios, swal },
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.vm.contact.name = 'Nombre'
    wrapper.vm.contact.email = 'Email@email.tk'
    wrapper.vm.contact.message = 'Mensaje de Contacto'

    await wrapper.find('form').trigger('submit.prevent')

    expect(wrapper.vm.loading).toBe(true)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.valid).toBe(true)
    expect(wrapper.vm.contact.name).toBe('')
    expect(wrapper.vm.contact.email).toBe('')
    expect(wrapper.vm.contact.message).toBe('')
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.valid).toBe(false)
    expect(wrapper.vm.loading).toBe(false)
    expect(swal).toHaveBeenCalledWith({
      title: `email-sent`,
      text: `will-contact`,
      icon: 'success',
      buttons: `close`,
    })
  })

  test('After submit form, if there is an error sending email, should show a sweetAlert dialog', async () => {
    store = createStore()

    axios.post.mockImplementation(() =>
      Promise.reject(new Error('Something went wrong'))
    )

    wrapper = await mount(Contact, {
      localVue,
      vuetify,
      store,
      mocks: { ...mocks, axios, swal },
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })

    await wrapper.setData({
      contact: {
        name: 'Nombre',
        email: 'email@email.com',
        message: 'Mensaje de Contacto',
      },
    })
    await wrapper.setData({ reCAPTCHA: true })

    await wrapper.vm.$nextTick()
    const textArea = wrapper.find('textarea')
    await textArea.setValue('Contact Form test')
    await wrapper.vm.$nextTick()
    await wrapper.find('form').trigger('submit.prevent')
    await wrapper.vm.$nextTick()
    expect(swal).toHaveBeenCalled()
    expect(swal).toHaveBeenCalledWith({
      title: `email-failed`,
      text: `try-again`,
      icon: 'error',
      buttons: `close`,
    })
  })

  test('If all input are valid SendButton should be able to be used', async () => {
    store = createStore()

    wrapper = await mount(Contact, {
      localVue,
      vuetify,
      store,
      mocks: { ...mocks, axios, swal },
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.setData({
      contact: {
        name: 'Nombre',
        email: 'email@email.com',
        message: 'Mensaje de Contacto',
      },
    })
    wrapper.setData({ reCAPTCHA: true })

    await wrapper.vm.$nextTick()
    const sendButton = wrapper.find('.send-button')
    const textArea = wrapper.find('textarea')
    await textArea.setValue('Contact Form test')

    expect(sendButton.props().disabled).toBe(false)
  })

  test("If captcha is not valid, SendButton shouldn't be able to be used", async () => {
    store = createStore()

    wrapper = await mount(Contact, {
      localVue,
      vuetify,
      store,
      mocks: { ...mocks, axios, swal },
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })
    wrapper.setData({
      contact: {
        name: 'Nombre',
        email: 'email@email.com',
        message: 'Mensaje de Contacto',
      },
    })
    wrapper.setData({ reCAPTCHA: false })

    await wrapper.vm.$nextTick()
    const sendButton = wrapper.find('.send-button')
    const textArea = wrapper.find('textarea')
    await textArea.setValue('Contact Form test')

    expect(sendButton.props().disabled).toBe(true)
  })

  test('Check callback existence when mounting the component', async () => {
    store = createStore()

    wrapper = await mount(Contact, {
      localVue,
      vuetify,
      store,
      mocks: { ...mocks, axios, swal },
      router,
      stubs: { NuxtLink: RouterLinkStub },
    })

    const callbackFunction =
      mocks.$fireModule.auth.RecaptchaVerifier.mock.calls[0][1].callback
    callbackFunction()

    expect(wrapper.vm.reCAPTCHA).toBe(true)
    expect(callbackFunction).toBeDefined()
  })
})
