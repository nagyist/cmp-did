import { createLocalVue, mount } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

import Logo from '@/components/public/Logo.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
let vuetify
let wrapper

// eslint-disable-next-line import/no-named-as-default-member
const $store = new Vuex.Store({
  state: {},
  getters: {
    'brand/logo': () => {
      return 'url'
    },
  },
})

describe('Logo', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  it('Expect computed to return the value in props', () => {
    wrapper = mount(Logo, {
      localVue,
      vuetify,
      propsData: {
        heightImage: '48px',
      },
      mocks: {
        $store,
      },
    })
    expect(wrapper.vm.heightLogo).toBe('48px')
  })
})
