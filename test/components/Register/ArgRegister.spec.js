import { createLocalVue, shallowMount, mount } from '@vue/test-utils'
import Vuetify from 'vuetify'
import Vuex from 'vuex'

import ArgRegister from '@/components/Register/ArgRegister.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
let vuetify
let wrapper
const $t = () => {}
// eslint-disable-next-line import/no-named-as-default-member
const store = new Vuex.Store({
  state: {},
  getters: {
    autenticarUser: () => {
      return true
    },
    'users/user': () => {
      return {
        first_name: 'Ramon',
        last_name: 'Paulino',
        email: '',
        password: '',
        dni: '0917763',
        public_address: '',
      }
    },
    'authconfig/autenticar': () => {
      return {
        autenticar: true,
        email: false,
        mobile: false,
        wallet: false,
        register: true,
        did: true,
      }
    },
  },
})
const fireModuleMock = {
  auth: {
    RecaptchaVerifier: jest.fn(() => ({
      render: jest.fn(() => Promise.resolve(1111)),
    })),
  },
}
const fireMock = {
  auth: {
    currentUser: jest.fn(() => ({
      render: jest.fn(() => Promise.resolve({ uid: '3435345' })),
    })),
  },
}

const $i18n = {
  locale: 'es',
}

describe('ArgRegister Tests', () => {
  beforeEach(() => {
    vuetify = new Vuetify()
    wrapper = shallowMount(ArgRegister, {
      localVue,
      store,
      vuetify,
      mocks: {
        $fireModule: fireModuleMock,
        $fire: fireMock,
        $t: (msg) => msg,
        $i18n,
      },
      propsData: {
        isRegister: true,
      },
    })
  })

  it('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy()
  })

  it('The validForm function emit boolean value for disabled form button', () => {
    wrapper.setData({ valid: true, checkbox: true })
    wrapper.setProps({ loading: false })
    expect(wrapper.vm.validForm).toBeFalsy()
  })

  it('Read inputs and variables to be empty on mounting', () => {
    const firstName = wrapper.find('#first-name')
    const lastName = wrapper.find('#last-name')
    const email = wrapper.find('#email')

    expect(firstName.text()).toBe('')
    expect(lastName.text()).toBe('')
    expect(email.text()).toBe('')

    expect(wrapper.vm.dataToRegister.first_name).toBe('')
    expect(wrapper.vm.dataToRegister.last_name).toBe('')
    expect(wrapper.vm.dataToRegister.email).toBe('')
  })

  it('When user is already logged in, the dataToRegister merges with the data from the user store', () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      state: {},
      getters: {
        autenticarUser: () => {
          return true
        },
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return {
            first_name: 'Ramon',
            last_name: 'Paulino',
            email: '',
            password: '',
            dni: '0917763',
            public_address: '',
          }
        },
        'authconfig/autenticar': () => {
          return {
            autenticar: true,
            email: false,
            mobile: false,
            wallet: false,
            register: true,
            did: true,
          }
        },
      },
    })
    const wrapper = mount(ArgRegister, {
      localVue,
      store,
      vuetify,
      mocks: {
        $fireModule: fireModuleMock,
        $fire: fireMock,
        $t,
      },
      propsData: {
        isRegister: true,
      },
      data() {
        return {
          dataToRegister: {
            first_name: '',
            last_name: '',
            email: 'joker@hotmail.com',
            password: '****',
            dni: '',
            public_address: '',
          },
        }
      },
    })

    expect(wrapper.vm.dataToRegister).toStrictEqual({
      first_name: 'Ramon',
      last_name: 'Paulino',
      email: '',
      password: '',
      dni: '0917763',
      public_address: '',
    })
  })

  test('When user is autenticated, the dataToRegister merges with the data from the autenticated store', () => {
    // eslint-disable-next-line import/no-named-as-default-member
    const store = new Vuex.Store({
      state: {},
      getters: {
        loggedIn: () => {
          return true
        },
        'users/user': () => {
          return false
        },
        autenticarUser: () => {
          return {
            first_name: 'Ramon',
            last_name: 'Paulino',
            email: '',
            password: '',
            dni: '0917763',
            public_address: '',
          }
        },
        'authconfig/autenticar': () => {
          return {
            autenticar: true,
            email: false,
            mobile: false,
            wallet: false,
            register: true,
            did: true,
          }
        },
      },
    })
    const wrapper = mount(ArgRegister, {
      localVue,
      store,
      vuetify,
      mocks: {
        $fireModule: fireModuleMock,
        $fire: fireMock,
        $t,
      },
      propsData: {
        isRegister: true,
      },
      data() {
        return {
          dataToRegister: {
            first_name: '',
            last_name: '',
            email: 'joker@hotmail.com',
            password: '****',
            dni: '',
            public_address: '',
          },
        }
      },
    })

    expect(wrapper.vm.dataToRegister).toStrictEqual({
      first_name: 'Ramon',
      last_name: 'Paulino',
      email: '',
      password: '',
      dni: '0917763',
      public_address: '',
    })
  })

  // test('if Complete values, change to next step', async () => {
  // console.log(store.getters.autenticar)
  // store.commit('SET_LOGIN_TYPES', {
  //   autenticar: true,
  //   email: false,
  //   mobile: false,
  //   wallet: false,
  //   register: true,
  //   did: true,
  // })
  // wrapper = mount(Register, {
  //   localVue,
  //   store,
  //   vuetify,
  //   mocks: {
  //     $fireModule: fireModuleMock,
  //     $t,
  //   },
  //   propsData: {
  //     isRegister: true,
  //   },
  // })
  // console.log(wrapper)
  //   console.log(wrapper.vm.$store.getters.autenticar)
  //   console.log(wrapper.find('#cuit').exists())
  //   const name = ''
  //   const lastName = ''
  //   const email = ''
  //   const password = ''
  //   const cuit = ''

  //   const nameInput = wrapper.find('#first-name')
  //   await nameInput.setValue(name)

  //   const lastnameInput = wrapper.find('#last-name')
  //   await lastnameInput.setValue(lastName)

  //   const emailInput = wrapper.find('#email')
  //   await emailInput.setValue(email)

  //   const passwordInput = wrapper.find('#password')
  //   await passwordInput.setValue(password)

  //   // console.log(wrapper.find('#cuit').exists())
  //   if (store.getters.autenticar) {
  //     const cuitInput = wrapper.find('#cuit')
  //     await cuitInput.setValue(cuit)
  //   }

  //   expect(wrapper.vm.dataToRegister.first_name).toBe(name)
  //   expect(wrapper.vm.dataToRegister.last_name).toBe(lastName)
  //   expect(wrapper.vm.dataToRegister.email).toBe(email)
  //   expect(wrapper.vm.dataToRegister.password).toBe(password)
  //   expect(wrapper.vm.dataToRegister.dni).toBe(cuit)

  //   expect(wrapper.vm.valid).toBeFalsy()
  //   expect(wrapper.vm.loading).toBeFalsy()
  //   expect(wrapper.find('#save-user').attributes('disabled')).toMatch(
  //     'disabled'
  //   )
  // })

  // test('Should active the button if form is not empty', async () => {
  //   const name = 'Pamela'
  //   const lastName = 'Martínez'
  //   const email = 'pamela@os.city'
  //   const cuit = 'holiholi'

  // const nameInput = wrapper.find('#first-name')
  // await nameInput.setValue(name)

  // const lastnameInput = wrapper.find('#last-name')
  // await lastnameInput.setValue(lastName)

  // const emailInput = wrapper.find('#email')
  // await emailInput.setValue(email)

  //   const cuitInput = wrapper.find('#cuit')
  //   await cuitInput.setValue(cuit)

  //   expect(wrapper.vm.dataToRegister.first_name).toBe(name)
  //   expect(wrapper.vm.dataToRegister.last_name).toBe(lastName)
  //   expect(wrapper.vm.dataToRegister.email).toBe(email)
  //   expect(wrapper.vm.dataToRegister.dni).toBe(cuit)

  //   expect(wrapper.vm.valid).toBeTruthy()
  //   wrapper.find('#save-user').trigger('click')
  // })
})
