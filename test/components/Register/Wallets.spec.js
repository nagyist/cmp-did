import { shallowMount } from '@vue/test-utils'
import Vuetify from 'vuetify'

import Wallets from '~/components/Register/Wallets.vue'

describe('Test Wallets', () => {
  let vuetify
  let wrapper

  beforeEach(() => {
    vuetify = new Vuetify()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('Snapshot should coincide with the previous created snapshot', () => {
    wrapper = shallowMount(Wallets, {
      vuetify,

      mocks: {
        $t: (msg) => msg,
      },
    })
    expect(wrapper.html()).toMatchSnapshot()
  })
})
