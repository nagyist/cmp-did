import Vuetify from 'vuetify'
import { shallowMount, createLocalVue, RouterLinkStub } from '@vue/test-utils'

import ProcedureCard from '@/components/Procedures/ProcedureCard.vue'

const localVue = createLocalVue()

describe('Testing Procedure Card', () => {
  let vuetify
  let wrapper

  beforeEach(() => {
    vuetify = new Vuetify()
  })
  test('should shorten the procedure description if length > 230 characters', () => {
    const description =
      'Pariatur culpa reprehenderit proident qui amet minim sunt amet. Consequat enim consequat magna voluptate ullamco et in excepteur velit esse excepteur cupidatat pariatur Lorem. Cupidatat dolor officia Lorem incididunt laboris laboris elit cupidatat ad enim irure do.'
    const mocks = {
      $store: {
        state: {
          loggedIn: true,
        },
      },
      $nuxt: {
        $route: {
          path: 'procedures',
        },
      },
    }
    wrapper = shallowMount(ProcedureCard, {
      localVue,
      vuetify,
      propsData: {
        shortDescription: description,
      },
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(description.length).toBeGreaterThan(230)
    expect(wrapper.vm.shortText).toBe(
      'Pariatur culpa reprehenderit proident qui amet minim sunt amet. Consequat enim consequat magna voluptate ullamco et in excepteur velit esse excepteur cupidatat pariatur Lorem. Cupidatat dolor officia Lorem incididunt laboris labor...'
    )
  })
  test("should'nt shorten the procedure description if length <= 230 characters", () => {
    const description =
      'Pariatur culpa reprehenderit proident qui amet minim sunt amet. Consequat enim consequat magna voluptate ullamco et in excepteur velit esse excepteur cupidatat pariatur Lorem.'
    const mocks = {
      $store: {
        state: {
          loggedIn: true,
        },
      },
      $nuxt: {
        $route: {
          path: 'procedures',
        },
      },
    }
    wrapper = shallowMount(ProcedureCard, {
      localVue,
      vuetify,
      propsData: {
        shortDescription: description,
      },
      mocks,
      stubs: { NuxtLink: RouterLinkStub },
    })
    expect(description.length).toBeLessThanOrEqual(230)
    expect(wrapper.vm.shortText).toBe(
      'Pariatur culpa reprehenderit proident qui amet minim sunt amet. Consequat enim consequat magna voluptate ullamco et in excepteur velit esse excepteur cupidatat pariatur Lorem.'
    )
  })
})
