require('dotenv').config('../.env')

if (process.env.FIREBASE_PROJECT_ID === 'mar_de_plata') {
  window.dataLayer = window.dataLayer || []
  function gtag() {
    // eslint-disable-next-line no-undef
    dataLayer.push(arguments)
  }
  gtag('js', new Date())
  gtag('config', 'G-9RFVFWDF62')
}
