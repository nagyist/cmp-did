export default function ({ store, redirect, route }) {
  if (!store.state.loggedIn && !isPublic(route.path)) {
    return redirect('/')
  }
}

function isPublic(path) {
  if (
    path === '/' ||
    path === '/login' ||
    path === '/contact' ||
    path === '/privacy' ||
    path === '/register' ||
    path === '/password-reset' ||
    path === '/email-action-handler' ||
    path === '/reset-password' ||
    path === '/server-error' ||
    path === '/what-is' ||
    path === '/digital-citizen' ||
    path === '/digital-citizen-levels' ||
    path === '/terms-of-service' ||
    path === '/notice-of-privacy' ||
    path === '/procedures' ||
    path.includes('/procedures/') ||
    path === '/procedures/public/clave-fiscal/steps' ||
    path === '/procedures/public/clave-fiscal/data-to-collect' ||
    path === '/procedures/public/clave-fiscal/description' ||
    path === '/procedures/public/clave-fiscal/requirements' ||
    path === '/procedures/public/inscripcion-ingresos-brutos-directos/steps' ||
    path ===
      '/procedures/public/inscripcion-ingresos-brutos-directos/data-to-collect' ||
    path ===
      '/procedures/public/inscripcion-ingresos-brutos-directos/description' ||
    path ===
      '/procedures/public/inscripcion-ingresos-brutos-directos/requirements' ||
    path === '/procedures/public/exclusion-o-retencion-bancaria/steps' ||
    path ===
      '/procedures/public/exclusion-o-retencion-bancaria/data-to-collect' ||
    path === '/procedures/public/exclusion-o-retencion-bancaria/description' ||
    path === '/procedures/public/exclusion-o-retencion-bancaria/requirements' ||
    path.includes('/citizen/certificates/digital-identity/')
  ) {
    return true
  } else {
    return false
  }
}
