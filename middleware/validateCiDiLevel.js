export default function ({ store, redirect }) {
  if (store.state.loggedIn && store.state.users.user.levelCidi < 2) {
    return redirect('/validateCiDiLevel')
  }
}
