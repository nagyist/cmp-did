export default function ({ store, redirect }) {
  if (
    store.state.loggedIn &&
    !store.state.users.user.public_address &&
    store.getters['authconfig/autenticar']
  ) {
    return redirect('/complete-data?step=3')
  }
}
