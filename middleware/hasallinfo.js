export default function ({ store, redirect }) {
  if (store.state.users.user.email && store.state.users.user.public_address) {
    return redirect('/')
  }
}
