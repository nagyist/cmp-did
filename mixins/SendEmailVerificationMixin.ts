import axios from 'axios'
import Vue from 'vue'
import _ from 'lodash'

import { getUserByEmail, User } from '@/services/User'

export default Vue.extend({
  name: 'SendEmailVerificationMixin',
  data() {
    return {
      urlEndpoints: process.env.OSCITY_ENDPOINTS_URL,
      userData: {
        user: {} as User,
      },
      loading: false,
      snackbar: {
        show: false,
        color: '',
        mode: '',
        timeout: 6000,
        text: '',
        right: '',
        top: '',
      },
    }
  },
  methods: {
    async sendEmail(
      userEmail: string,
      subject: string,
      title: string,
      link: string,
      content: string
    ) {
      try {
        await axios.post(`${this.urlEndpoints}/sendEmail`, {
          personalizations: [
            {
              to: [
                {
                  email: userEmail,
                },
              ],
              dynamic_template_data: {
                user: userEmail,
                title,
                subject,
                link,
                content,
                button: this.$t('continue'),
              },
              subject,
            },
          ],
        })
      } catch (error: any) {
        throw new Error(error)
        // console.error(error)
      }
    },
    async sendMultipleEmails(personalizations: Array<Object>) {
      try {
        await axios.post(`${this.urlEndpoints}/sendEmail`, {
          personalizations,
          dynamic_template_data: {
            title: `To receive your certificate on Blockchain, it must be authorized`,
            subject: 'Verification Email - UNICEF',
            content: `Visit this link to complete the process`,
            button: `Continue`,
          },
        })
      } catch (error: any) {
        throw new Error(error)
      }
    },
    async createTokenAuthorization(email: string): Promise<string> {
      try {
        const link = await axios.post(
          `${this.urlEndpoints}/createTokenUnicef`,
          {
            email,
            currentUrl: this.$store.state.currentUrl,
          }
        )
        return link.data
      } catch (error) {
        console.error(error)
        throw error
      }
    },
    async createMultipleTokenAuthorization(
      selected: Array<String>
    ): Promise<Array<Object>> {
      try {
        const arrayTokens = await axios.post(
          `${this.urlEndpoints}/createMultipleTokensUnicef`,
          {
            selected,
            currentUrl: this.$store.state.currentUrl,
          }
        )
        return arrayTokens.data
      } catch (error) {
        console.error(error)
        return []
      }
    },
    async sendEmailVerificationSengrid(
      modeEmail: String,
      email?: String,
      uid?: String,
      language?: String
    ): Promise<void> {
      this.loading = true
      try {
        const userEmail = email || this.$fire.auth.currentUser.email
        let linkToVerify = null
        const subject =
          modeEmail === 'verifyEmail'
            ? this.$t('verifica-tu-correo')
            : this.$t('restablece-tu-contrasena')
        const existUser = await getUserByEmail(this.$fire, userEmail)
        const userFirstName = existUser.docs[0]
          ? existUser.docs[0].data().first_name.split(' ')
          : ''
        const userName = _.capitalize(userFirstName[0])
        if (uid || !existUser.empty) {
          linkToVerify = await axios.post(`${this.urlEndpoints}/createToken`, {
            uid: uid || existUser.docs[0].id,
            currentUrl: this.$store.state.currentUrl,
            mode: modeEmail,
          })
        } else {
          this.snackbar.show = true
          this.snackbar.text = 'El email no esta registrado.'
          this.snackbar.color = 'error'
        }
        if (linkToVerify) {
          await axios.post(`${this.urlEndpoints}/sendEmail`, {
            personalizations: [
              {
                to: [
                  {
                    email: userEmail,
                  },
                ],
                dynamic_template_data: {
                  user: userName,
                  title: subject,
                  subject,
                  link: linkToVerify.data,
                  content:
                    language === 'en'
                      ? 'Continue with this link to finish the process'
                      : 'Visita este vínculo para continuar con tu proceso.',
                  button: language === 'en' ? 'Continue' : 'Continuar',
                  warning:
                    modeEmail === 'verifyEmail'
                      ? ''
                      : 'Si no solicitaste restablecer tu contraseña, podés ignorar este correo.',
                },
                subject,
              },
            ],
          })
          this.snackbar.show = true
          this.snackbar.text =
            language === 'en'
              ? 'An email has been sent'
              : 'Se te ha enviado un correo electrónico.'
          this.snackbar.color = 'success'
        }
      } catch {
        this.snackbar.show = true
        this.snackbar.text =
          language === 'en'
            ? 'Email had not been sent'
            : 'El mail no pudo ser enviado. Por favor intenta nuevamente.'
        this.snackbar.color = 'error'
      }
      this.loading = false
    },
  },
})
