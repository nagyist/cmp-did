import Vue from 'vue'

import { mapGetters } from 'vuex'

import { updateCertificate } from '@/services/Certificates'

export default Vue.extend({
  name: 'RevokeCertificatesMixin',
  data() {
    return {}
  },
  computed: {
    ...mapGetters({
      user: 'users/user',
    }),
  },
  methods: {
    getCertificate(firebaseCollection: string) {
      const matchValue = this.user.uid
      return this.$fire.firestore
        .collection(firebaseCollection)
        .where('id_user', '==', matchValue)
        .where('is_certified', '==', true)
        .get()
    },
    revokeCertificates(collection: string) {
      const revokedData = this.getCertificate(collection)
        .then((certs: any) => {
          let item = null
          if (!certs.empty) {
            certs.forEach(async (doc: any) => {
              if (!doc.is_revocated) {
                item = { ...doc.data() }
                item.estado_origen = 'Revocado'
                item.is_revocated = true
                const id = item.id_reference
                await updateCertificate(this.$fire, id, item, collection)
              }
            })
          }
          return { item, collection }
        })
        .catch(() => {
          return null
        })
      return revokedData
    },
  },
})
