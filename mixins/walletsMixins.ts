import Vue from 'vue'
import Web3 from 'web3'
import swal from 'sweetalert'
import { WalletLink } from 'walletlink'
import detectEthereumProvider from '@metamask/detect-provider'

declare global {
  interface Window {
    web3: any
    uportconnect: any
  }
}
export default Vue.extend({
  name: 'Wallets',
  data() {
    return {
      typeWallet: '',
      public_address: '',
      web3: null as any,
      INFURA_KEY: process.env.INFURA_KEY_ROPSTEN,
      walletLink: Object(),
      snackbar: {
        show: false,
        color: '',
        mode: '',
        timeout: 600000,
        text: '',
        right: '',
        top: '',
      },
    }
  },
  methods: {
    detachCoinbase(): void {
      this.walletLink.disconnect()
    },
    loadCoinBase(): void {
      const APP_NAME = 'OsCity Demosburg'
      const APP_LOGO_URL = '@/assets/images/logo.png'
      const ETH_JSONRPC_URL = this.INFURA_KEY
      const CHAIN_ID = 3

      const walletLink = new WalletLink({
        appName: APP_NAME,
        appLogoUrl: APP_LOGO_URL,
        darkMode: false,
      })
      this.walletLink = walletLink
      const ethereum = walletLink.makeWeb3Provider(ETH_JSONRPC_URL, CHAIN_ID)
      const web3 = new Web3(ethereum)
      this.typeWallet = 'Coinbase'
      ethereum.enable().then((accounts: any) => {
        web3.eth.defaultAccount = accounts[0]
        this.public_address = accounts[0]
        this.$emit('change', this.public_address)
        this.$emit('typew', this.typeWallet)
        this.$emit('walletLink', this.walletLink)
      })
    },
    uportSession() {
      const Connect = window.uportconnect
      const uport = new Connect('OsCity Demosburg')
      const reqObj = {
        requested: ['name', 'email'],
        notifications: false,
      }
      uport.requestDisclosure(reqObj)
      uport.onResponse('disclosureReq').then((res: any) => {
        const did = res.payload.did
        this.public_address = did.split(':')[2]
        this.typeWallet = 'Uport'
        this.$emit('change', this.public_address)
        this.$emit('typew', this.typeWallet)
      })
    },
    async loadWeb3Provider() {
      const provider = await detectEthereumProvider()
      if (provider) {
        try {
          await (provider as any).enable()
          this.web3 = new Web3(provider)
          this.$emit('web3', this.web3)
          this.typeWallet = 'Metamask'
        } catch (error) {
          this.snackbar.show = true
          this.snackbar.text = 'El usuario cancelo el modal'
          this.snackbar.color = 'warning'
          this.typeWallet = ''
        }
        this.$emit('typew', this.typeWallet)
        this.loadPubKeyData(provider)
      } else if (
        !window.web3 &&
        !provider &&
        this.$vuetify.breakpoint.smAndDown
      ) {
        const link = document.createElement('div')
        link.innerHTML = `<p>Si estás desde un dispositivo móvil, prueba instalando Metamask Browser desde la tienda de tu plataforma</p>
            <div class="meta_btn_container">
              <a class='metamask_link' target='_blank' href='https://play.google.com/store/apps/details?id=io.metamask'>
                <button class="logo_metamask_playstore">
                </button>
              </a>
              <a class='metamask_link' target='_blank' href='https://apps.apple.com/us/app/metamask/id1438144202'>
                <button class="logo_metamask_ios">
                </button>
              </a>
            </div>
          `
        swal({
          title: 'Metamask no encontrado',
          icon: 'warning',
          content: link,
          button: 'Aceptar',
        })
      } else {
        const link = document.createElement('div')
        link.innerHTML =
          "Metamask no se encuentra instalado en tu navegador, por favor instalalo desde su  <a class='metamask_link' target='_blank' href='https://metamask.io/'>página oficial</a>"
        swal({
          title: 'Metamask no encontrado',
          icon: 'warning',
          content: link,
          button: 'Aceptar',
        })
      }
    },
    async loadPubKeyData(ethProvider: any): Promise<void> {
      const accounts = await ethProvider.request({ method: 'eth_accounts' })
      this.public_address = accounts[0]
      this.$emit('change', this.public_address)
      ethProvider.on('accountsChanged', (accounts: any) => {
        this.public_address = accounts[0]
        if ((this as any).typew) {
          this.$emit('change', this.public_address)
        }
      })
    },
  },
})
