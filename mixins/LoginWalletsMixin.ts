import Vue from 'vue'
import axios from 'axios'

import { signInCustomToken } from '~/services/Firebase'
import { getUser, getUserByPublicAddress } from '~/services/User'

export default Vue.extend({
  name: 'LoginWalletsMixin',
  data() {
    return {
      snackbar: {
        show: false,
        color: '',
        mode: '',
        timeout: 600000,
        text: '',
        right: '',
        top: '',
      },
      urlEndpoints: process.env.OSCITY_ENDPOINTS_URL,
      email: '',
    }
  },
  methods: {
    async loginOtherWallet() {
      ;(this as any).loading = true
      this.$emit('wait', (this as any).loading)
      let user
      const exist = await getUserByPublicAddress(this.$fire, {
        publicAddress: (this as any).public_address,
      })
      if (exist.docs.length > 0) {
        user = (await getUser(this.$fire, exist.docs[0].id)).data()
      }
      if (user && user.email) {
        if (this.email === user.email) {
          if (user.public_address && user.uid) {
            this.onPublicAddress(user.uid)
            ;(this as any).loading = false
            this.$emit('wait', (this as any).loading)
            this.$router.push({ path: '/' })
          } else {
            this.snackbar.color = 'error'
            this.snackbar.show = true
            this.snackbar.text =
              'Error al traer información dle usaurio, intenta de nuevo'
          }
        } else {
          this.snackbar.color = 'error'
          this.snackbar.show = true
          this.snackbar.text =
            'El mail no coincide en el registro con la llave pública'
        }
      } else {
        this.snackbar.color = 'error'
        this.snackbar.show = true
        this.snackbar.text = 'La llave pública es incorrecta'
      }
    },
    async loginMetamask() {
      ;(this as any).loading = true
      this.$emit('wait', (this as any).loading)
      let user
      let userDataVar: any
      let usertoVerify
      const exist = await getUserByPublicAddress(this.$fire, {
        publicAddress: (this as any).public_address,
      })
      if (exist.docs.length > 0) {
        user = (await getUser(this.$fire, exist.docs[0].id)).data()
      }
      if (user && user.email) {
        if (this.email === user.email) {
          if (user.public_address && user.uid) {
            await axios
              .post(`${this.urlEndpoints}/generateNonce`, {
                user,
              })
              .then(async () => {
                userDataVar = (
                  await getUser(this.$fire, exist.docs[0].id)
                ).data()
              })
            await this.handleSignMessage(
              userDataVar.public_address,
              userDataVar.nonce
            )
              .then((res: any) => {
                usertoVerify = res
              })
              .catch(() => {
                this.snackbar.color = 'error'
                this.snackbar.show = true
                this.snackbar.text = 'Ocurrió un error al firmar con metamask'
              })
            await axios
              .post(`${this.urlEndpoints}/verificationSignature`, usertoVerify)
              .then((response: any) => {
                if (response.status) {
                  this.onPublicAddress(response.data.user.uid)
                }
              })
              .catch(() => {
                this.snackbar.color = 'error'
                this.snackbar.show = true
                this.snackbar.text =
                  'Ocurrió un error al verificar la firma de metamask'
                ;(this as any).loading = false
                this.$emit('wait', (this as any).loading)
              })
          } else {
            this.snackbar.color = 'error'
            this.snackbar.show = true
            this.snackbar.text =
              'Error al traer información dle usaurio, intenta de nuevo'
            ;(this as any).loading = false
            this.$emit('wait', (this as any).loading)
          }
        } else {
          this.snackbar.color = 'error'
          this.snackbar.show = true
          this.snackbar.text =
            'El mail no coincide en el registro con la llave pública'
          ;(this as any).loading = false
          this.$emit('wait', (this as any).loading)
        }
      } else {
        this.snackbar.color = 'error'
        this.snackbar.show = true
        this.snackbar.text = 'La llave pública es incorrecta'
        ;(this as any).loading = false
        this.$emit('wait', (this as any).loading)
      }
    },
    async handleSignMessage(publicAddress: string, nonce: any) {
      try {
        let message = ''
        if (this.$i18n.locale === 'es') {
          message =
            'Estás a punto de hacer uso de firma electrónica en blockchain. Haz click en firmar para usar tu llave pública y llave privada de forma segura'
        } else {
          message =
            'You are about to use your digital signature on blockchain. click on sign to securely use your public and private key'
        }
        const signature = await (this as any).web3.eth.personal.sign(
          `${message}: ${nonce}`,
          publicAddress,
          '' // MetaMask will ignore the password argument here
        )

        return { publicAddress, signature, message }
      } catch (err) {
        ;(this as any).loading = false
        this.$emit('wait', (this as any).loading)
        throw new Error(
          'You need to sign the message to be able to log in.' + err
        )
      }
    },
    async onPublicAddress(uid: any) {
      let credential
      await axios
        .post(`${this.urlEndpoints}/createCustomToken`, { uid })
        .then((response: any) => {
          credential = response.data.customToken
        })
        .catch(() => {
          ;(this as any).loading = false
          this.$emit('wait', (this as any).loading)
          this.snackbar.color = 'error'
          this.snackbar.show = true
          this.snackbar.text = 'Error al crear el Token de inicio de sesión'
        })
      await signInCustomToken(this.$fire, { credential })
      ;(this as any).loading = false
      this.$emit('wait', (this as any).loading)
      this.$router.push({ path: '/' })
    },
  },
})
