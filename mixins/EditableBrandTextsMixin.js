import Vue from 'vue'
import _ from 'lodash'
import axios from 'axios'
import { getLastBrand, getBrand, updateBrand } from '~/services/Brand'

export default Vue.extend({
  name: 'EditableTextsMixin',
  methods: {
    async toggleInput(field, element) {
      this[field] = true
      await this.$nextTick()
      this.$refs[element].focus()
    },
    async getBrandSettings() {
      this.loading = true

      try {
        await getLastBrand(this.$fire).then(async (result) => {
          if (result.empty) {
            // No result, use default brand from store
            this.brandSettings = _.cloneDeep(this.$store.state.brand.brand)
            await this.createBrandSettings()
          } else {
            const data = {
              ...result.docs[0].data(),
              id: result.docs[0].id,
            }
            this.$store.commit('brand/SET_BRAND', data)
            this.brandSettings = _.cloneDeep(data)
          }
        })
        this.loading = false
      } catch (error) {
        console.error(error)
        this.loading = false
      }
    },
    async createBrandSettings() {
      this.loading = true
      const flatTextValues = {
        ...this.brandSettings,
      }
      const brandSettings = await axios.post(
        `${this.urlEndpoints}/saveCollectionData`,
        {
          collection: 'brand',
          data: flatTextValues,
        }
      )
      const brandText = (await getBrand(this.$fire, brandSettings.id)).data()
      brandText.id = brandSettings.id
      this.$store.commit('brand/SET_BRAND', brandText)
      this.brandSettings = _.cloneDeep(this.$store.state.brand.brand)
      this.loading = false
    },
    async updateBrand() {
      this.loading = true
      try {
        await updateBrand(this.$fire, this.brandSettings.id, this.brandSettings)
        const brand = await getBrand(this.$fire, this.brandSettings.id)
        this.$store.commit('brand/SET_BRAND', brand.data)
        this.clearStore()
      } catch {
        this.snackbar.show = true
        this.snackbar.text = 'No se pudo guardar la actualización'
        this.snackbar.color = 'error'
      }
      this.loading = false
    },
  },
})
