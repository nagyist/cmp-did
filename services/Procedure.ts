export const getAllProcedures = async ($fire: any) => {
  return await $fire.firestore.collection('procedures').get()
}

export const getLastProcedure = async ($fire: any) => {
  return await $fire.firestore.collection('procedures').limit(1).get()
}

export const getProcedure = async ($fire: any, id: string) => {
  return await $fire.firestore.collection('procedures').doc(id).get()
}

export const setProcedure = async ($fire: any, data: any) => {
  return await $fire.firestore.collection('procedures').add(data)
}

export const updateProcedure = async ($fire: any, id: string, body: any) => {
  const data = { ...body }
  return await $fire.firestore
    .collection('procedures')
    .doc(id)
    .set(data, { merge: true })
}
