/* eslint-disable camelcase */
import moment from 'moment'

/**
 * @todo Homologate User firestore properties to camelcase
 */
export interface User {
  birthdate: string
  created: string
  first_name: string
  last_name: string
  foto: string
  gender: string
  idUser: string
  phone: string
  picture: string
  public_address: string
  scholarity: string
  uid: string
  location: {
    lat: string
    lng: string
    address: string
    custom: string
    street: string
    num_ext: string
    num_int: string
    neighborhood: string
    betweenStreets: string
    references: string
  }
}

export const setUserDoc = async ($fire: any, id: string, body: any) => {
  const data = {
    uid: id,
    ...body,
    created: moment().format(),
  } as User
  return await $fire.firestore
    .collection('users')
    .doc(id)
    .set(data, { merge: true })
}

export const getUserByPublicAddress = async (
  $fire: any,
  { publicAddress }: any
) => {
  return await $fire.firestore
    .collection('users')
    .where('public_address', '==', publicAddress)
    .get()
}

export const getUserByEmail = async ($fire: any, email: string) => {
  return await $fire.firestore
    .collection('users')
    .where('email', '==', email)
    .get()
}

export const getUser = async ($fire: any, id: string) => {
  return await $fire.firestore.collection('users').doc(id).get()
}

export const deleteUserCollection = async ($fire: any, uid: string) => {
  return await $fire.firestore.collection('users').doc(uid).delete()
}

export const getUsers = async ($fire: any) => {
  return await $fire.firestore.collection('users').get()
}

export const setUserCidiLevel = async ($fire: any, id: string) => {
  return await $fire.firestore
    .collection('users')
    .doc(id)
    .update({ levelCidi: 2 })
}
