export const getLastBrand = async ($fire: any) => {
  return await $fire.firestore.collection('brand').limit(1).get()
}

export const getBrand = async ($fire: any, id: string) => {
  return await $fire.firestore.collection('brand').doc(id).get()
}

export const setBrand = async ($fire: any, data: any) => {
  return await $fire.firestore.collection('brand').add(data)
}

export const updateBrand = async ($fire: any, id: string, body: any) => {
  const data = { ...body }
  return await $fire.firestore
    .collection('brand')
    .doc(id)
    .set(data, { merge: true })
}
