import moment from 'moment'
export const setCatalogs = async ($fire: any, docs: [], collection: string) => {
  const batch = $fire.firestore.batch()
  docs.forEach((doc: any) => {
    doc.created = moment().format()

    const docRef = $fire.firestore.collection(collection).doc()
    batch.set(docRef, doc)
  })
  return await batch.commit()
}

export const getProvinces = async ($fire: any) => {
  return await $fire.firestore.collection('provinces').get()
}

export const getNationalities = async ($fire: any) => {
  return await $fire.firestore.collection('nationalities').get()
}

export const getActivities = async ($fire: any) => {
  return await $fire.firestore.collection('activities').get()
}

export const getDepartmentsByProvince = async (
  $fire: any,
  codigoProvincia: string
) => {
  return await $fire.firestore
    .collection('departments')
    .where('code_provincia', '==', codigoProvincia)
    .get()
}

export const getLocalitiesByDepartment = async (
  $fire: any,
  codigoDepartamento: string,
  codigoProvincia: string
) => {
  return await $fire.firestore
    .collection('localities')
    .where('code_departamento', '==', codigoDepartamento)
    .where('code_provincia', '==', codigoProvincia)
    .get()
}

export const getExemptTypes = () => {
  const jsonFile = require('@/static/js/catalogs/tipo_exento.json')

  return jsonFile.tipo_exento
}

export const getCivilStatus = () => {
  const jsonFile = require('@/static/js/catalogs/estado_civil.json')

  return jsonFile.estado_civil
}

export const getAdressTypes = () => {
  const jsonFile = require('@/static/js/catalogs/tipo_domicilio.json')

  return jsonFile.tipo_domicilio
}

export const getIvaSituation = () => {
  const jsonFile = require('@/static/js/catalogs/situacion_iva.json')

  return jsonFile.situacion_iva
}

export const getActivityDescription = async (
  $fire: any,
  codeActividad: string
) => {
  return await $fire.firestore
    .collection('activities')
    .where('code_actividad', '==', codeActividad)
    .get()
}
