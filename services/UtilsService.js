export default {
  /**
   * Format a numeric value to defind digits and return it in locale string
   * @param {number} number Value to format
   * @param {integer} digits Quantity of digits after floating point
   */
  formatNumber(number, digits = 1) {
    if (Number.isNaN(Number(number))) {
      throw new TypeError('"number" param must be a number')
    }
    if (!Number.isInteger(digits)) {
      throw new TypeError('"digits" param must be an integer')
    }
    if (digits < 0) {
      throw new TypeError('"digits" param must greater than zero')
    }
    return Number(number.toFixed(digits)).toLocaleString()
  },
}
