/* eslint-disable */
import Vue from 'vue'

import { getUser } from '~/services/User'

export const state = () => ({
  user: {},
  currentUrl: '',
})

export const mutations = {
  SET_ID_TOKEN(state, idToken) {
    state.idToken = idToken
  },
  SET_USER_UID(state, uid) {
    state.userUid = uid
  },
  SET_USER: (state, user) => {
    state.user.type = user.type ? user.type : null
    state.user = { ...state.user, ...user }
  },
}

export const actions = {
  async fetchUser({ commit }, authData) {
    if (authData && authData.authUser) {
      if (authData.idToken && authData.userUid) {
        commit('SET_ID_TOKEN', authData.idToken)
        commit('SET_USER_UID', authData.userUid)
      }
      const user = (await getUser(this.$fire, authData.authUser.uid)).data()
      if (user) {
        commit('SET_USER', {
          ...user,
        })
      }
    } else {
      commit('SET_USER', {})
    }
  },
  sendCode({ dispatch }, dataRecaptcha) {
    return new Promise((resolve, reject) => {
      this.$fire.auth
        .signInWithPhoneNumber(dataRecaptcha.phone, dataRecaptcha.verifier)
        .then((confirmationResult) => {
          window.confirmationResult = confirmationResult
          resolve(confirmationResult)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
}

export const getters = {
  user: (state) => {
    return state.user
  },
  publicAddress: (state) => {
    return state.user.public_address
  },
  userName(state) {
    return state.user.first_name
  },
  isVerified(state) {
    return state.user.activated
  },
  isOperator(state) {
    if (
      state.user &&
      state.user.type &&
      _.includes(['superadmin', 'operator'], state.user.type)
    ) {
      return true
    }
    return false
  },
}
