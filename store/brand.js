import axios from 'axios'
import Vuetify from '~/plugins/vuetify'
import { getLastBrand } from '~/services/Brand'

export const state = () => ({
  brand: {
    app_name: '',
    city_splash_primary:
      'https://firebasestorage.googleapis.com/v0/b/demosburg-dev.appspot.com/o/brand%2Fimages%2Fdefault_splash_primary.png?alt=media&token=b4feafa8-08ba-4396-9fc0-1fbbed2cf5e4',
    city_splash_secondary:
      'https://firebasestorage.googleapis.com/v0/b/demosburg-dev.appspot.com/o/brand%2Fimages%2Fdefault_splash_secondary.png?alt=media&token=616244e7-e306-498a-a3b3-3d138c15f10f',
    city_splash_login: '',
    city_url: 'demo.os.city',
    city_name: '',
    copyright_footer: '',
    favicon: '',
    logo: null,
    logo_provincia_misiones: null,
    primary_color: '#e5528e',
    secondary_color: '#ad57b2',
    header_color: '#ffffff',
    footer_color: '#ffffff',
    footer_contact_button_color: '',
    footer_background_type: 'lineal',
    footer_bg_primary: '',
    footer_bg_secondary: '',
    footer_logo: '',
    footer_text_color: '',
    textHeaderColor: '#ffffff',
    textFooterColor: '#ffffff',
    heightLogo: '40px',
    additional_color: '#d6d6d6',
    about_title: 'Acerca de',
    about_content: '',
    contact_title: 'Contacto',
    contact_info: '',
    hero_bg_primary: '#5d306f',
    hero_bg_secondary: '#bd0b4d',
    terms: false,
    privacy: false,
    termsAndConditions: '',
    privayPolicy: '',
    needsInfo: true,
    buttonColorLogin: '#43475C',
    mobileStepThree: '',
    firstImageStepThree: '',
    secondImageStepThree: '',
    thirdImageStepThree: '',
    header_bg_primary: '#0096da',
    header_bg_secondary: '#035d98',
    header_button_color: '#0088CC',
    header_background_type: 'lineal',
    errorImage: '',
    hero_background_type: 'lineal',
    hero_bg_image: '',
    levelUpImage: '',
    validateLevelImage: '',
    appStoreDowload: '',
    qrImage: '',
    loaded: false,
    avatar_text_color: '#FFFFFF',
  },
})

export const mutations = {
  SET_BRAND(state, brand) {
    state.brand = brand
    if (state.brand.primary_color) {
      Vuetify.theme.themes.light.primary = state.brand.primary_color
    }
    if (state.brand.secondary_color) {
      Vuetify.theme.themes.light.primary = state.brand.secondary_color
    }
  },
}

export const actions = {
  async get({ commit, rootState }) {
    await getLastBrand(this.$fire).then(async (result) => {
      const brand = rootState.brand.brand
      if (!result.empty) {
        const data = {
          ...result.docs[0].data(),
          id: result.docs[0].id,
        }
        commit('SET_BRAND', { ...brand, ...data })
      } else {
        const urlEndpoints = process.env.OSCITY_ENDPOINTS_URL
        await axios.post(`${urlEndpoints}/saveCollectionData`, {
          collection: 'brand',
          data: brand,
        })
      }
    })
    commit('SET_BRAND', { ...rootState.brand.brand, loaded: true })
  },
}

export const getters = {
  brand(state) {
    return state.brand
  },
  needsInfo(state) {
    return state.brand.needsInfo
  },
  logo(state) {
    return state.brand.logo
  },
  logoProvinciaMisiones(state) {
    return state.brand.logo_provincia_misiones
  },
  privacy(state) {
    return state.brand.privacy
  },
  terms(state) {
    return state.brand.terms
  },
  buttonColorLogin(state) {
    return state.brand.buttonColorLogin
  },
  logoFooter(state) {
    return state.brand.footer_logo
  },
  getHeaderColor(state) {
    return state.brand.header_color
  },
  getFooterColor(state) {
    return state.brand.footer_color
  },
  getHeaderTextColor(state) {
    return state.brand.textHeaderColor
  },
  getFooterButtonColor(state) {
    return state.brand.footer_contact_button_color
  },
  getFooterTextColor(state) {
    return state.brand.textFooterColor
  },
  getHeightLogo(state) {
    return state.brand.heightLogo
  },
  getPrimaryColor(state) {
    return state.brand.primary_color
  },
  getSecondaryColor(state) {
    return state.brand.secondary_color
  },
  getHeroPrimaryColor(state) {
    return state.brand.hero_bg_primary
  },
  getHeroSecondaryColor(state) {
    return state.brand.hero_bg_secondary
  },
  getMobileStepThree(state) {
    return state.brand.mobileStepThree
  },
  getFirstImageStepThree(state) {
    return state.brand.firstImageStepThree
  },
  getSecondImageStepThree(state) {
    return state.brand.secondImageStepThree
  },
  getThirdImageStepThree(state) {
    return state.brand.thirdImageStepThree
  },
  getButtonHeaderColor(state) {
    return state.brand.header_button_color
  },
  getErrorImage(state) {
    return state.brand.errorImage
  },
  getLevelUpImage(state) {
    return state.brand.levelUpImage
  },
  getValidateLevelImage(state) {
    return state.brand.validateLevelImage
  },
  getAvatarTextColor(state) {
    return state.brand.avatar_text_color
  },
}
