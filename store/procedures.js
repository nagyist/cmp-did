import { getAllProcedures } from '~/services/Procedure'

export const state = () => ({
  procedures: [],
})

export const mutations = {
  SET_PROCEDURES(state, procedures) {
    state.procedures = [...procedures]
  },
}

export const actions = {
  async get({ commit }) {
    try {
      const result = await getAllProcedures(this.$fire)
      const procedures = []
      if (!result.empty) {
        result.forEach((doc) => {
          procedures.push({ id: doc.id, ...doc.data() })
        })
      }
      commit('SET_PROCEDURES', procedures)
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  },
}

export const getters = {
  getProcedures(state) {
    return state.procedures
  },
  getActiveProcedures(state) {
    const activeProcedures = state.procedures.filter(
      (procedure) => procedure.active === true
    )
    return activeProcedures
  },
}
