/* eslint-disable */

import _ from 'lodash'
import axios from 'axios'

import { getLastSettings } from '~/services/Settings'

export const state = () => ({
  setting: {
    email_contact: ''
  }
})

export const mutations = {
  SET_SETTING(state, setting) {
    state.setting = setting
  }
}

export const actions = {
  async get({ commit, rootState }) {
    await getLastSettings(this.$fire).then(async (res) => {
      let settings = rootState.setting.setting
      if (!res.empty) {
        const data = res.docs[0].data()
        commit('SET_SETTING', data)
      } else {
        const urlEndpoints = process.env.OSCITY_ENDPOINTS_URL
        await axios.post(
          `${urlEndpoints}/saveCollectionData`,
          {
            collection: 'settings',
            data: settings,
          }
        )
      }
    })
  }
}
