/* eslint-disable */

import _ from 'lodash'
import axios from 'axios'

import { getAuthConfig } from '~/services/AuthConfig'

export const state = () => ({
  databaseLoad: null,
  authconfig: {
    autenticar: false,
    email: true,
    mobile: false,
    wallet: false,
    register: true,
    did: false,
    withAnalytics: false,
    createNewAdmin: false,
  },
})

export const mutations = {
  SET_LOGIN_TYPES(state, authconfig) {
    state.authconfig = authconfig
  },
  SET_DATABASE(state, authconfig) {
    state.databaseLoad = authconfig
  },
}

export const actions = {
  async get({ commit, rootState }) {
    await getAuthConfig(this.$fire).then(async (res) => {
      commit('SET_DATABASE', true)
      let authconfig = rootState.authconfig.authconfig
      if (!res.empty) {
        authconfig = res.docs[0].data()
        commit('SET_LOGIN_TYPES', authconfig)
      } else {
        const urlEndpoints = process.env.OSCITY_ENDPOINTS_URL
        await axios.post(`${urlEndpoints}/saveCollectionData`, {
          collection: 'authConfig',
          data: authconfig,
        })
      }
    })
  },
}

export const getters = {
  autenticar(state) {
    return state.authconfig.autenticar
  },
  email(state) {
    return state.authconfig.email
  },
  mobile(state) {
    return state.authconfig.mobile
  },
  wallet(state) {
    return state.authconfig.wallet
  },
  register(state) {
    return state.authconfig.register
  },
  isDID(state) {
    return state.authconfig.did
  },
  databaseLoad(state) {
    return state.databaseLoad
  },
  withAnalytics(state) {
    return state.authconfig.withAnalytics
  },
  createNewAdmin(state) {
    return state.authconfig.createNewAdmin
  },
}
